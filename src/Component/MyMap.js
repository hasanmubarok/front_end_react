import React, {Component} from "react";
import {Link} from "react-router-dom";
import "../App.css";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

class MyMap extends Component{
    render(){
        const style = {
            width: '500px',
            height: '500px',
            display:'block',
          }
        return(
            // element map
            <Map google={this.props.google} 
            style={style}
            zoom={15}
            initialCenter={{
                lat: -7.9675821,
                lng:  112.6138938
              }}
            >
 
                <Marker onClick={this.onMarkerClick}
                        name={'Current location'} />
        
                <InfoWindow onClose={this.onInfoWindowClose}>
                    <div>
                    </div>
                </InfoWindow>
            </Map>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyBnOC2cYnLyaaYXtnd_IEQWZLkqvg0tqoE")
  })(MyMap)