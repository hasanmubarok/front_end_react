import React, { Component } from "react";
import axios from "axios";
import { connect } from "unistore/react";
import NotifList from "./NotifList";
import { Badge } from "reactstrap";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'

class Notification extends Component {

    notify = () => toast("Hai kamu dapat pemberitahuan baru, Check Notifikasi donk!");

    state = {
        notifcationData: [],
        notifCount: 0,
        isHidden: false,
        readCount: 0,
    }

    toggleShow = () => {
        this.setState({ isHidden: !this.state.isHidden });
        if (this.state.isHidden) {
            this.updateNotif()
        }
    }

    updateNotif = () => {
        const self = this;
        let token = self.props.token
        //Offer
        axios
            .put("https://api.temenmakan.online/api/user/notifcation/modify", {}, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
            })
            .catch((err) => {
                console.log(err)
            })
    }

    getNotif = () => {
        const self = this;
        let token = self.props.token
        //Offer
        axios
            .get("https://api.temenmakan.online/api/user/notifcation", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ notifcationData: response.data });
                console.log("Notif Data", response.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    componentDidMount = () => {
        //Autorefresh 
        this.intervalID = setInterval(
            () => this.tick(),
            1500
        );
        this.getNotif();
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }

    tick() {
        this.setState({
            time: new Date().toLocaleString()
        });
        this.getNotif();
        //Set count
        this.setState({
            readCount:
                this.state.notifcationData.filter((item) => {
                    return item.status == "unread";
                }).length
        });
        if (this.state.readCount != this.state.notifCount) {
            this.notify();
            // alert("Ada notif baru");
            // toast.success("Wow so easy !");
            this.setState({ notifCount: this.state.readCount });
        }
    }

    render() {

        const { notifcationData } = this.state;
        return (
            <div>
                <i class="far fa-bell" onClick={this.toggleShow}>
                    <Badge className="badge-notif" color="warning" pill>
                        {this.state.notifCount}
                    </Badge>
                </i>

                {this.state.isHidden ?
                    <div className="detail-notifikasi dropdown-menu-right">
                        <PerfectScrollbar>
                            {notifcationData.map((item, key) => {
                                return <NotifList id={item.id} sender={item.id_sender} type={item.id_type} object={item.id_object} />
                            })}
                        </PerfectScrollbar>
                    </div>
                    : null}

            </div>
        );
    }
}
export default connect("token")(Notification);
