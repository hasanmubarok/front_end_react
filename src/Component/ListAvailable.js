import React, {Component} from "react";
import axios from "axios";
import {connect} from "unistore/react";
import {actions} from "../store";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class ListAvailable extends Component {

    state = {
        jarak : 0,
    }

    invitePerson = (e) => {
        let partner_id =e.target.value
        const self = this;
        let token = this.props.token
        axios
        .post("https://api.temenmakan.online/api/user/invite", { partner_id : partner_id}, 
            {headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            self.props.history.push("/request" );
            toast("Selamat kamu berhasil mengundang seorang teman :D, Tunggu Konfirmasinya Ya ;) Silahkan cek status di My Invitation");
            //Send Notif
            axios
            .post("https://api.temenmakan.online/api/user/notifcation", {id_receiver: partner_id, id_type:1 },{
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                console.log("Notif Data",response.data)
            })
            .catch((err) => {
                console.log(err)
            })
        })
    }

    componentDidMount = () => {
        const self = this;
        axios
        .get('https://maps.googleapis.com/maps/api/distancematrix/json?origins='+this.props.userDetail['latitude']+','+this.props.userDetail['longitude']+'&destinations='+this.props.ltd+','+this.props.lng+'&key=AIzaSyBnOC2cYnLyaaYXtnd_IEQWZLkqvg0tqoE')
        .then(function(response){
            self.setState({ jarak: response.data.rows[0].elements[0].distance.text })
        })
        .catch((err) => {
            console.log(err)
        })
    }

    render() {
        const route = "/profileother/tentang/" + this.props.id
        return (
            <div className="row">
                <div className="col-md-8 col-sm-8 picture-teman-makan">
                    <Link to={route}><img
                        width="50px"
                        height="50px"
                        src={this.props.urlPict}
                    /></Link>
                    <div className="span-teman-makan">
                        <Link to={route}><span><strong>{this.props.name}</strong></span></Link>
                        <br/><span> {this.props.age}<small>th</small>, {this.props.gender}</span>
                        <br />
                        <span>Pengen makanan {this.props.wanttoEat}</span>
                        <br/>
                        <span style={{color:"#00a896"}}><small>{this.state.jarak} dari lokasi anda saat ini</small></span>
                    </div>
                </div>
                <div className="col-md-4 col-sm-4 ikon-teman text-right button-teman">
                    <button type="button" name="personID" className="btn btn-info" style={{borderRadius: "20px"}} value={this.props.id} onClick={this.invitePerson}>
                    <i class="fas fa-utensils"></i> Ajak Makan
                    </button>
                </div>
            </div>
        )
    }
}

export default withRouter(connect("token, userDetail") (ListAvailable));