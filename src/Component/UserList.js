import React, {Component} from "react";
import axios from "axios";
import InviteStatusOffer from "./InviteStatusOffer.1";
import {connect} from "unistore/react";
import InviteStatusRequest from "./InviteStatusRequest";
import { Link } from "react-router-dom";

class UserList extends Component {
    
    render() {
        return (
            <div className="col-md-6  participant-list">
                <div className="participant-item border rounded">
                    <div className="avatar-participant ">
                        <Link to={{ pathname: "/profileother/tentang/" + this.props.id }}>
                            <img style={{borderRadius:"50%"}} src={this.props.urlPict} />
                        </Link>
                    </div>
                    <div className="desc-participant ">
                        <Link to={{ pathname: "/profileother/tentang/" + this.props.id }}>
                            <span><strong>{this.props.name}</strong></span><br />
                        </Link>
                        <span>{this.props.gender}, 21 tahun.</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect("token") (UserList);