import React, {Component} from "react";
// import NavBar from "../Component/navbar";
import { withRouter } from 'react-router-dom';
import { connect } from "unistore/react";
import { actions } from "../store";
import {Link} from "react-router-dom";
import "../App.css";
import axios from "axios";

class LsbPhoto extends Component{
    state = {
        userData: [],
        ratingData: [],
        totalRating: 0
    };    

    componentDidMount = () => {
        const self = this;
        let token = this.props.token
        let id = this.props.id;
        if(!id){
        id = this.props.userDetail.id
        }
        axios
        .get("https://api.temenmakan.online/api/user/" + id, {
            headers: { Authorization: "Bearer " + token }
        })
        .then(function (response) {
            self.setState({
                userData: response.data
            });
        })
        .catch((err) => {
            console.log(err)
        })
        axios
            .get("https://api.temenmakan.online/api/users/rating/" + id, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ ratingData: response.data });
                console.log("rating",response.data)
                let count = 0;
                {
                    self.state.ratingData.map((item) => {
                        self.setState({ totalRating: self.state.totalRating += item.rating });
                        count += 1;
                    })
                }
                self.setState({ totalAllRating: self.state.totalRating / count });

            })
            .catch((err) => {
                console.log(err)
            })
    }
    render(){
        const userDetail = this.state.userData;
        const buttonEditTampil = this.props.buttonEditTampil;
        let { ratingData, totalAllRating } = this.state;
        var stars;
        var userLevel;
        if ( totalAllRating > 1 && totalAllRating < 2.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>;
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>JawatKuliner</strong></span><br /></div>
        }
        else if ( totalAllRating >= 2.5 && totalAllRating < 3.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>;
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>TemenKuliner</strong></span><br /></div>
        }
        else if ( totalAllRating >= 3.5 && totalAllRating < 4.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>;
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>KoncoKuliner</strong></span><br /></div>
        }
        else if ( totalAllRating >= 4.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>;
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>SobatKuliner</strong></span><br /></div>
        }
        else {
            stars =
            <img width="1px" height="1px" src="https://image.flaticon.com/icons/svg/149/149222.svg"/>
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>KolegaKuliner</strong></span><br /></div>
        }
        if(buttonEditTampil === true){
            return(
                <div className="col-md-3 col-xs-4 profile-left">
                    <div className="status-makan text-content">
                        <div className="text-center profil-kiri">
                            <img src={userDetail.urlpict} />
                            <h4>{userDetail.username}</h4>
                            {/* <span>{userDetail.cityname}</span><br />
                            <span>Indonesia</span><br/> */}
                            {/* <hr/> */}
                            <Link to="/updateprofile">
                                <button className="edit-profile-button">Edit Profileku</button><br/>
                            </Link>
                            {userLevel}
                            {/* <span className="grade-status" style={{color:"#996515"}}><strong>{userLevel}</strong></span><br /> */}
                            <Link to="/profile/referensi">
                                <span>{ratingData.length} referensi</span><br />
                            </Link>
                                <div className="stars">{stars}</div>
                            <span className="last-login-status">Login terakhir: {this.props.userDetail.lastlogin}</span>
                            {/* <Link to="/updateprofile">
                                <button className="edit-profile-button">Edit Profileku</button>
                            </Link> */}
                        </div>
                    </div>
                </div>
        )
    } else{
        return(
            <div className="col-md-3 col-xs-4 profile-left">
                    <div className="status-makan text-content">
                        <div className="text-center profil-kiri">
                            <img src={userDetail.urlpict} />
                            <h4>{userDetail.username}</h4>
                            {/* <span>{userDetail.cityname}</span><br /> */}
                            {/* <span>Indonesia</span> */}
                            <hr/>
                            {userLevel}
                            <Link to="/profile/referensi">
                                <span>{ratingData.length} referensi</span><br />
                            </Link>
                                <div className="stars">{stars}</div>
                            <span className="last-login-status">Login terakhir: {this.props.userDetail.lastlogin}</span>
                        </div>
                    </div>
                </div>
        )
    }
    }
}

export default connect("userDetail, token, is_login", actions)(withRouter(LsbPhoto));
