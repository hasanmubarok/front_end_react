import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";
import { withRouter } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import Notification from "./Notification";

class NavBar extends Component {
    state = {
        search: this.props.match.params.query
    };

    componentDidMount = () => {
        this.props.getUserDetail(this.props.token);
    };

    inputChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    searchQuery = e => {
        let search = this.state.search;
        this.props.history.push("/");
        this.props.history.push("/search/" + search);
    };

    render() {
        const userDetail = this.props.userDetail;
        return (
            <nav className="navbar navbar-expand-lg navbar-light" style={{ position: 'fixed', width: '100%', zIndex: '1', backgroundColor: '#00a896', fontFamily: 'roboto', fontWeight: '500', fontSize: '18px' }}>
                <Link to="/home" class="navbar-brand mr-auto">
                    <img src="https://i.ibb.co/TLgGVYw/garpu-putih-4.png" width="auto" height="50px" alt="Logo Temen-Makan" />
                </Link>
                <div class="navbar-collapse mx-auto w-auto justify-content-end" id="navbarNavAltMarkup">
                    <div className="insearch">
                        <input
                            name="search"
                            type="text"
                            className="textbox"
                            placeholder="cari orang, acara, atau post"
                            onChange={this.inputChange}
                            value={this.state.search}
                        />
                        <button
                            title="Search"
                            value=""
                            type="submit"
                            className="button"
                            onClick={this.searchQuery}
                        >
                            <i className="fa fa-search" />
                        </button>
                    </div>
                </div>
                <button
                    class="navbar-toggler ml-auto"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span class="navbar-toggler-icon" />
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto" style={{ display: "flex", alignItems: "left" }}>
                        <li class="nav-item">
                            <Link to="/request" class="nav-link" style={{ color: 'white' }}>Request?</Link>
                        </li>
                        <li class="nav-item">
                            <Link to="/post" class="nav-link" style={{ color: 'white' }}>Makan Apa?</Link>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style={{ color: 'white' }}>
                                <Notification />
                            </a>
                        </li>
                        <li class="nav-item dropdown photo-navbar">
                            <a
                                // class="nav-link dropdown-toggle"
                                href="#"
                                id="navbarDropdownMenuLink"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                <img width="50px" height="50px" src={userDetail.urlpict} alt="profil photo" />
                            </a>
                            <div
                                class="dropdown-menu dropdown-menu-right"
                                aria-labelledby="navbarDropdownMenuLink"
                            >
                                <Link class="dropdown-item" to="/profile/tentang">
                                <i class="fas fa-user-circle"></i> Profile
                                    </Link>
                                <Link className="dropdown-item" to="/" onClick={() => this.props.handleLogout()}><i class="fas fa-sign-out-alt"></i> Logout</Link>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

// export default NavBar;
export default connect(
    "userDetail, token, is_login",
    actions
)(withRouter(NavBar));
