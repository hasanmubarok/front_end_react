import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";

class NavbarProfile extends Component {
    render() {
        return (
            <div>
                <div className="row text-center navbar-profile">
                    <div className="col-md-3">
                        <Link to="/profile/tentang">Tentangku</Link>
                    </div>
                    <div className="col-md-3">
                        <Link to="/profile/post">Post</Link>
                    </div>
                    <div className="col-md-3">
                        <Link to="/profile/gallery">Foto</Link>
                    </div>
                    <div className="col-md-3">
                        <Link to="/profile/referensi">Referensi</Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default NavbarProfile;
