import React, {Component} from "react";
import {geolocated} from 'react-geolocated';
import { connect } from "unistore/react";
import { actions } from "../store";

class GeolocationProps extends Component{
    state = {
        longitude : "",
        latitude : "",
    }

    componentDidMount = () => {
        this.props.coords && alert(this.props.coords.latitude)
      }
      
    render() {
      !this.state.longitude && this.props.coords && this.setState({longitude: this.props.coords.longitude})
      return !this.props.isGeolocationAvailable
      ? <div>Your browser does not support Geolocation</div>
      : !this.props.isGeolocationEnabled
      ? <div>Geolocation is not enabled</div>
      : this.props.coords
      ? <table>
                <tbody>
                  <tr><td>latitude</td><td>{() => this.props.getLocation(this.state.longitude, this.state.latitude)}{this.props.coords.latitude}</td></tr>
                  <tr><td>longitude</td><td>{this.props.coords.longitude}</td></tr>
                  {/* <tr><td>altitude</td><td>{this.props.coords.altitude}</td></tr> */}
                  {/* <tr><td>heading</td><td>{this.props.coords.heading}</td></tr> */}
                  {/* <tr><td>speed</td><td>{this.props.coords.speed}</td></tr> */}
                </tbody>
              </table>
              : <div>Getting the location data&hellip; </div>;
            }

      // return "ini longitude "+ this.state.longitude +" Hehe"
    // }
}

export default geolocated({
    positionOptions: {
      enableHighAccuracy: true,
    },
    userDecisionTimeout: 5000,
  }) (connect(
    "userDetail, token, latitude, longitude",
    actions
  )(GeolocationProps))