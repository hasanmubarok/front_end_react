import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class RatingList extends Component {
    render() {
        var stars;
        if ( this.props.rating > 1 && this.props.rating < 2.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
        }
        else if ( this.props.rating >= 2.5 && this.props.rating < 3.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
        }
        else if ( this.props.rating >= 3.5 && this.props.rating < 4.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
        }
        else if ( this.props.rating >= 4.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
        }
        else {
            stars =
            <img width="1px" height="1px" 
            // src="https://image.flaticon.com/icons/svg/149/149222.svg"
            src="https://image.flaticon.com/icons/svg/148/148841.svg"
            />
        }
        return (
            <div class="row rating-list border" style={{borderRadius:"20px"}}>
                <div class="col-md-1 kolom-avatar-rating">
                    <Link to={"/profileother/tentang/"+ this.props.id}><img className="rounded-circle" src={this.props.urlPict} /></Link>
                </div>
                <div class="col-md-11">
                    <span>Rating dari 
                    <Link to={"/profileother/tentang/"+ this.props.id}><span><strong> {this.props.name}</strong></span><br /></Link>
                    <div className="stars-small">{stars}</div></span>
                    <span className="date-time">{this.props.created_at}</span><br />
                    <p>{this.props.comment}</p>
                </div>
            </div>
        )
    }
}

export default RatingList;