import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";

class NavbarProfileOther extends Component {
    render() {
        return (
            <div>
                <div className="row text-center navbar-profile">
                    <div className="col-md-3 col-sm-3">
                        <Link to={{ pathname: '/profileother/tentang/' + this.props.id }}>
                            Tentangku
                        </Link>
                    </div>
                    <div className="col-md-3 col-sm-3">
                        <Link to={{ pathname: '/profileother/post/' + this.props.id }}>
                            Post
                        </Link>
                    </div>
                    <div className="col-md-3 col-sm-3">
                        <Link to={{ pathname: '/profileother/gallery/' + this.props.id }}>
                            Foto
                        </Link>
                    </div>
                    <div className="col-md-3 col-sm-3">
                        <Link to={{ pathname: '/profileother/referensi/' + this.props.id }}>
                            Referensi
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}


export default NavbarProfileOther;