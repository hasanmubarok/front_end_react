import React, { Component } from "react";
import "../App.css";
import axios from "axios";
import {Link} from "react-router-dom";

class Tentangku extends Component {
    state = {
        ratingData: [],
    }

    componentDidMount = () => {
        const self = this;
        let token = self.props.token
        let id = this.props.userDetail.id
        //Offer
        axios
            .get("https://api.temenmakan.online/api/users/rating/" + id, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ ratingData: response.data });
                self.setState({ type: "request" });
                console.log("data rating",response.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }
    render() {
        let id = this.props.userDetail.id
        let { ratingData } = this.state;
        var occupation;
        if (this.props.userDetail.occupation==="" || this.props.userDetail.occupation==="None") {
            occupation = <Link to="/updateprofile">[Data Pekerjaan Belum Ada]</Link>
        } else {
            occupation = this.props.userDetail.occupation
        }
        var address;
        if (this.props.userDetail.address==="" || this.props.userDetail.address===null) {
            address = <Link to="/updateprofile">[Data Alamat Asal Belum Ada]</Link>
        } else {
            address = "Dari " + this.props.userDetail.address
        }
        var descself;
        if (this.props.userDetail.descself==="" || this.props.userDetail.descself===null) {
            descself = <Link to="/updateprofile">[Ceritakan Mengenai Dirimu Disini]</Link>
        } else {
            descself = this.props.userDetail.descself
        }console.log("help occ", this.props.userDetail.descself)
        return (
            <div>
                <hr/>
                <div className="row tinjauan">
                    <h4>Tinjauan</h4>
                    <div className="col-md-6">
                        <span> <img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/1081/1081947.svg"/> {ratingData.length} Referensi</span><br />
                        <br/>
                        <span> <img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/196/196131.svg"/> {this.props.userDetail.age}th, {this.props.userDetail.gender}</span><br />
                        <br/>                        
                        <span> <img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/265/265683.svg"/> Member sejak {this.props.userDetail.membersince}</span><br />
                        <br/>
                    </div>
                    <div className="col-md-6">
                        <span><img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/387/387569.svg"/>  {occupation}</span><br />
                        <br/>                        
                        <span><img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/189/189060.svg"/>  {address}</span><br />
                    </div>
                </div>
                <hr/>
                <div className="row tentangku">
                    <h4>Tentangku</h4>
                    <p>{descself}</p>
                </div>
            </div>
        )
    }
}


export default Tentangku;