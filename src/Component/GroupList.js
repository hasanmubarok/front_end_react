import React, {Component} from "react";
import axios from "axios";
import InviteStatusOffer from "./InviteStatusOffer.1";
import {connect} from "unistore/react";
import InviteStatusRequest from "./InviteStatusRequest";
import { Link } from "react-router-dom";

class GroupList extends Component {
    
    render() {
        return (
            <div id="group-section" className="row">
                <div className="col-md-8 col-sm-8">
                    <div className="row">
                        <div className="group-detail">
                            <div className="span-group-makan">
                                <h5 style={{color:"#00a896"}}>{this.props.name}</h5>
                                <span>{this.props.place}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-sm-4 ikon-teman text-right button-teman">
                        <Link to={{pathname: '/group/' + this.props.id}}>
                    <button type="button" className="btn btn-info" style={{borderRadius: "20px"}}><i class="fas fa-info-circle"></i> Lihat Detail
                    </button>
                    </Link>
                </div>
                <div className="row" style={{margin: 0, width: "100%"}}>
                    <div className="col-md-2">
                        <img width="30px" height="30px" src="https://image.flaticon.com/icons/svg/138/138773.svg"/>
                    </div>
                    <div style={{margin:"auto"}} className="col-md-7">
                        <span>{this.props.date}</span>
                    </div>
                    <div style={{margin: "auto", textAlign:"right"}}className="col-md-3">
                        <span>
                            {this.props.membercount} <i class="fas fa-users" />
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect("token") (GroupList);