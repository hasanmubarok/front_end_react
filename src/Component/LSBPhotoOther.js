import React, { Component } from "react";
import { withRouter, Redirect, Link } from 'react-router-dom';
import { connect } from "unistore/react";
import { actions } from "../store";
import axios from "axios";
import NotFound from "../Pages/NotFound";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const buttonstyle ={
    backgroundColor: "#00a896",
    border: 0,
    marginTop: "10px",
    borderRadius: "20px"
}

const buttonStyleInvite ={
    backgroundColor: "#e2e1e0",
    color: "black",
    border: 0,
    marginTop: "10px",
    borderRadius: "20px"
}

const topAndBottomDistanceStyle = {
    paddingTop: 10
}

class LSBPhotoOther extends Component{

    state={
        ratingData: [],
        totalRating: 0,
        totalAllRating: 0,
        otherUserDetail: []
    }

    invitePerson = (e) => {
        let partner_id =this.props.id
        const self = this;
        let token = this.props.token
        axios
        .post("https://api.temenmakan.online/api/user/invite", { partner_id : partner_id}, 
            {headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            self.props.history.push("/request" );
            toast("Selamat kamu berhasil mengundang seorang teman :D, Tunggu Konfirmasinya Ya ;) Silahkan cek status di My Invitation");
            //Send Notif
            axios
            .post("https://api.temenmakan.online/api/user/notifcation", {id_receiver: partner_id, id_type:1 },{
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                console.log("Notif Data",response.data)
            })
            .catch((err) => {
                console.log(err)
            })
        })
    }

    componentDidMount = () => {
        this.props.getUserDetail(this.props.token)
        let id = this.props.id;
        console.log("id props", id)
        const self = this;
        const url = "https://api.temenmakan.online/api/user/" + id
        const auth = "Bearer " + this.props.token
        axios
        .get(url, {headers: {Authorization : auth}})
        .then((response) => {
            self.setState({
                otherUserDetail: response.data
            })
            // console.log("data", response.data)
        })
        .catch((err) => {
            console.log(err)
        })
        
        axios
            .get("https://api.temenmakan.online/api/users/rating/" + this.props.match.params.id, {
                headers: { Authorization: "Bearer " + this.props.token }
            })
            .then(function (response) {
                self.setState({ ratingData: response.data });
                // console.log("this",response.data)
                let count = 0;
                {
                    self.state.ratingData.map((item, key) => {
                        self.setState({ totalRating: self.state.totalRating += item.rating });
                        count += 1;
                    })
                }
                self.setState({ totalAllRating: self.state.totalRating / count });

            })
            .catch((err) => {
                console.log(err)
            })
    };

    render(){
        // console.log("props di render", this.props)
        let id = this.props.match.params.id;
        if(!id){
        id = this.props.userDetail.id
        }
        const { otherUserDetail, ratingData, totalAllRating } = this.state;
        const userDetail = this.props.userDetail;
        console.log("helph",otherUserDetail)
        var stars;
        var userLevel;
        var buttoninvite;
        if ( otherUserDetail.user_status === "Makan Bareng? Quy!") {
            buttoninvite = <div><button style={buttonstyle} className="btn btn-primary" onClick={this.invitePerson}>Invite</button></div>
        }
        else {
            // buttoninvite = <div><span>User sedang tidak available untuk diajak makan bareng</span></div>
            buttoninvite = <div><button style={buttonstyle} disabled className="btn btn-primary" onClick={this.invitePerson}>Invite</button></div>
        }
        if ( totalAllRating > 1 && totalAllRating < 2.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>JawatKuliner</strong></span><br /></div>
        }
        else if ( totalAllRating >= 2.5 && totalAllRating < 3.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>TemenKuliner</strong></span><br /></div>
        }
        else if ( totalAllRating >= 3.5 && totalAllRating < 4.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>KoncoKuliner</strong></span><br /></div>
        }
        else if ( totalAllRating >= 4.5 ) {
            stars =
            <div>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
                <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/>
            </div>
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>SobatKuliner</strong></span><br /></div>
        }
        else {
            stars =
            <img width="1px" height="1px" src="https://image.flaticon.com/icons/svg/149/149222.svg"/>
            userLevel = <div><span className="grade-status" style={{color:"#000000"}}><strong>KolegaKuliner</strong></span><br /></div>
        }

        if (this.props.is_login) {
            // if (otherUserDetail.username === userDetail.username) {
            //     return (
            //         <Redirect to={{ pathname: "/profile" }} />
            //     )
            // } else{
                return(
                    <div className="col-md-3 col-xs-4 profile-left">
                                    <div className="status-makan text-content">
                                        <div className="text-center profil-kiri">
                                            <img src={otherUserDetail.urlpict} />
                                            <h4>{otherUserDetail.username}</h4>
                                            <span style={{color:"#00a896"}}><small>{this.props.jarak} dari lokasi anda saat ini</small></span>
                                            {/* <span>{otherUserDetail.cityname}</span><br />
                                            <span>Indonesia</span> */}
                                        </div>
                                        <div className="text-center">
                                            {/* <button id="upgrade-verifikasi"> Upgrade untuk Verifikasi</button><br /> */}
                                            <Link to={{ pathname: '/chat/' + otherUserDetail.id }}>
                                                <button style={buttonstyle} className="btn btn-primary">Chat</button>
                                            </Link><br />
                                                <button style={buttonStyleInvite} className="btn btn-primary" onClick={this.invitePerson}>Invite</button>
                                                {buttoninvite}
                                            <div style={{marginTop:"20px", marginBottom:"20px"}}>
                                                {userLevel}
                                                {/* <span className="grade-status"><strong>Food Junkie</strong></span><br /> */}
                                            <Link to={"/profileother/referensi/" + otherUserDetail.id}>
                                                <span>{ratingData.length} referensi</span><br />
                                            </Link>
                                                <div className="stars">{stars}</div>
                                            <span>Login terakhir: {otherUserDetail.lastlogin}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                )
            // }
        }
    }
}

export default connect("userDetail, token, is_login", actions)(withRouter(LSBPhotoOther));
