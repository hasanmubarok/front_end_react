import React, {Component} from "react";
import axios from "axios";
import {connect} from "unistore/react";
import CheckRating from "./CheckRating";
import swal from "sweetalert2";


class InviteStatusOffer extends Component {
    state = {
        rating : 1,
        comment : "",
    }

    inputChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    SendRating = () => {        
        const self = this;
        let token = this.props.token
        axios
        .post("https://api.temenmakan.online/api/users/rating",{invite_id: self.props.id, comment: self.state.comment, rating : self.state.rating},{
            headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            // alert("Terima Kasih")
            swal("Terima Kasih")
        })
        .catch((err) => {
            // alert("Anda telah memberikan Rating :(")
            // swal("Anda telah memberikan Rating :(")
        })  
    }

    FinishRequest = (e) => {
        const self = this;
        let token = this.props.token
        //Offer
        axios
            .put("https://api.temenmakan.online/api/user/invite/change/" + e.target.name, { status: "Finish" }, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                // alert("Selamat :D Yuk beri rating ke partner makanmu")
                swal("Selamat :D Yuk beri rating ke partner makanmu")
            })
            .catch((err) => {
                console.log(err)
            })
    }
    
    render() {
        if(this.props.status == "Unconfirmed"){
            return (
                <div class="button-respon">
                    <button className="form-control" style={{backgroundColor:"#00a896", border:0, borderRadius:"20px", color: "white", height:"40px"}} name={this.props.id} disabled>{this.props.status}</button>
                </div>
            )
        }else if(this.props.status == "Accepted"){
            return (
                <div class="button-respon">
                <button className="form-control" style={{backgroundColor:"#00a896", border:0, borderRadius:"20px", color: "white", height:"40px", width:"100px"}} name={this.props.id} onClick={(e) => this.FinishRequest(e)}>Selesai</button>
                </div>
            )
        }else if(this.props.status == "Finish"){
            return (
                // <div class="button-respon">
                    <CheckRating id={this.props.id}/>
                // </div>
            )
        }else {
            return (
                <div class="button-respon">
                    {/* {this.props.status} */}
                </div>
            )
        }
    }
}

export default connect("token") (InviteStatusOffer);