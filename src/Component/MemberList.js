import React, {Component} from "react";
import axios from "axios";
import InviteStatusOffer from "./InviteStatusOffer.1";
import {connect} from "unistore/react";
import InviteStatusRequest from "./InviteStatusRequest";
import { Link } from "react-router-dom";

class MemberList extends Component {
    
    render() {
        return (
            <div className="col-md-6  participant-list">
                <div style={{marginBottom:"20px"}}>
                <div className="participant-item border" style={{borderRadius:"20px"}}>
                    <div className="avatar-participant ">
                        <Link to={{ pathname: "/profileother/tentang/" + this.props.id }}>
                            <img style={{borderRadius:"50%"}} src={this.props.urlPict} />
                        </Link>
                    </div>
                    <div className="desc-participant ">
                        <Link to={{ pathname: "/profileother/tentang/" + this.props.id }}>
                            <span><strong>{this.props.name}</strong>
                            <span className="badge badge-success">
                            {this.props.id == this.props.admin ? 'Admin' : ""}
                            </span>
                            </span><br />
                        </Link>
                        <span>{this.props.age}th, {this.props.gender}</span>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

export default connect("token") (MemberList);