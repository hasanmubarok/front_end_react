import React, { Component } from "react";
import axios from "axios";
import { connect } from "unistore/react";
import { Link } from "react-router-dom";

class NotifList extends Component {
    state = {
        infoUser: ""
    };

    componentDidMount = () => {
        const self = this;
        const url = "https://api.temenmakan.online/api/user/" + self.props.sender;
        const auth = "Bearer " + self.props.token;
        axios
            .get(url, { headers: { Authorization: auth } })
            .then(response => {
                self.setState({
                    infoUser: response.data
                });
                console.log("data", response.data);
            })
            .catch(err => {
                console.log(err);
            });
    };

    render() {
        let type = this.props.type;
        if (type == 2) {
            return (
                <div class="notif-wrapper">
                    <Link to={"/chat/" + this.props.sender}>
                        <img alt="avatar-user"
                            class="img-notif"
                            src={this.state.infoUser ? this.state.infoUser.urlpict : ""}
                        />
                        <span classname="info-wrapper">
                             Chat baru dari{" "}
                            {this.state.infoUser ? this.state.infoUser.username : ""}{" "}
                        </span>
                    </Link>
                </div>
            );
        } else if (type == 1) {
            return (
                <div class="notif-wrapper">
                    <Link to={"/request"}>
                        <img alt="avatar-user"
                            class="img-notif"
                            src={this.state.infoUser ? this.state.infoUser.urlpict : ""}
                        />
                        <span classname="info-wrapper">
                             Ajakan baru dari{" "}
                            {this.state.infoUser ? this.state.infoUser.username : ""}{" "}
                        </span>
                    </Link>
                </div>
            );
        } else {
            return (
                <div class="notif-wrapper">
                    <Link to={"//post/" + this.props.object}>
                        <span classname="info-wrapper">
                            Komentar baru dari{" "}
                            {this.state.infoUser ? this.state.infoUser.username : ""}{" "}
                        </span>
                        <img
                            class="img-notif"
                            src={this.state.infoUser ? this.state.infoUser.urlpict : ""}
                        />{" "}
                    </Link>
                </div>
            );
        }
    }
}
export default connect("token")(NotifList);
