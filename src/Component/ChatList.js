import React, {Component} from "react";
import axios from "axios";

class ChatList extends Component {
    render() {
        if(this.props.id == this.props.sender_id){
            return (
                <div className="col-md-12 order chat-list">
                    <div className="conversation-item ">
                        <div className="avatar-guest ">
                            <img src={this.props.urlPict} />
                        </div>
                        <div className="desc-chat ">
                            <span><strong>{this.props.sender}</strong></span><br />
                            <div className="chat-text-block ">
                                <div className="chat-text-cover ">
                                    <span>{this.props.message}</span>
                                </div>
                            </div>
                            <span className="chat-time">Dikirim pada : {this.props.time}</span>
                        </div>
                    </div>
                </div>
            )
        }else{
            return (
                <div class="col-md-12  chat-list">
                    <div class="conversation-item ">
                        <div class="avatar-self ">
                            <img src={this.props.urlPict} />
                        </div>
                        <div class="desc-chat text-right ">
                            <span><strong>{this.props.sender}</strong></span><br />
                            <div class="chat-text-block ">
                                <div class="chat-text-cover ">
                                    <span>{this.props.message}</span>
                                </div>
                            </div>
                            <span class="chat-time">Dikirim pada : {this.props.time}</span>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default ChatList;