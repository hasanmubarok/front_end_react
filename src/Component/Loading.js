import React, { Component } from "react";
import axios from "axios";
import NavBar from "../Component/navbar";

class Loading extends Component {
    render() {
        return (
            <div>
                <NavBar />
            <div className="row">
                <div className="loadings text-center">
                    <img src="https://media.giphy.com/media/lYhl5UtFnPqe4c6To9/giphy.gif" />
                </div>
            </div>
            </div>
        );
    }
}
export default Loading;
