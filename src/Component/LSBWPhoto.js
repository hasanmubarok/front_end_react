import React, {Component} from "react";
// import NavBar from "../Component/navbar";
import "../App.css";
import axios from "axios";
import { connect } from "unistore/react";
import swal from 'sweetalert2';

class LsbwPhoto extends Component{

    state = {
        userData : "",
        status : "",
        wanttoEat : "",
    }

    inputChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    componentDidMount = () => {
        const self = this;
        let token = this.props.token
        //Get Users Available
        axios
        .get("https://api.temenmakan.online/api/user/status", {
            headers: { Authorization: "Bearer " + token }
        })
        .then(function (response) {
            self.setState({ userData: response.data });
            self.setState({ status: response.data.status });
        })
    }

    componentWillMount = () => {
        this.detail = this.props.userDetail;
        this.setState({
            wanttoEat: this.detail.wanttoeat,
        })
    }

    updateStatus = (e) => {
        const self = this;
        let token = this.props.token
        //Get Users Available
        axios
        .put("https://api.temenmakan.online/api/user/status", {status : self.state.status, wanttoEat : self.state.wanttoEat} ,{
            headers: { Authorization: "Bearer " + token }
        })
        .then(function (response) {
            // alert("Berhasil Update Status")
            swal({
                position: 'top-center',
                type: 'success',
                title: 'Berhasil memperbarui data',
                showConfirmButton: false,
                timer: 1700
              })
        })
    }

    render(){
        const makanan = this.state.userData.wanttoEat;
        return(
            <div className="col-md-3 lsbwphoto">
                <div className="status-makan text-content">
                    <span>
                        <h4><strong>{this.props.userDetail.username}</strong></h4>
                    </span>
                    {/* <br /> */}
                    {/* <span>{this.props.userDetail.cityname}</span>
                    <br />
                    <span>Indonesia</span> */}
                    <hr />
                    <div className="status-available">
                        <form>
                            Status:
                            <select name="status" className="form-control" id="sel1" onChange={this.inputChange}>
                                <option selected>{this.state.userData.status}</option>
                                <option>Lagi Pengen Makan Sendiri Aja</option>
                                <option>Makan Bareng? Quy!</option>
                            </select>
                        </form>
                    </div>
                    <div className="row status-available">
                        <div
                            className="col-md-12"
                            id="lagi-ingin-makan"
                        >
                            {/* <span>Pengen makan : {makanan}<br/>  */}
                            <span>Ingin makan apa?<br/>
                            {/* (Last Value) */}
                            </span>
                        </div>
                        <div
                            className="col-md-12"
                            id="option-ingin-makan"
                        >
                            <input style={{borderTopLeftRadius:"20px", borderBottomLeftRadius:"20px", border: "1px", borderColor:"rgba(0,0,0,.2)", borderStyle:"solid", height:"40px"}} name="wanttoEat" type="text" placeholder="Ingin makan apa?" value={this.state.wanttoEat} onChange={this.inputChange}/>
                            <button style={{backgroundColor:"#00a896", border:0, borderTopRightRadius:"20px", borderBottomRightRadius:"20px", color:"white", height:"40px", width:"89px"}}onClick={this.updateStatus}>Perbarui</button>
                        </div>
                    </div>
                </div>
                {/* <div className="status-makan text-content">
                    <h4>PROFILKU</h4>
                    <hr />
                    <meter value="0.6">60%</meter>
                    <br />
                    <span className="lengkapi-profile">Lengkapi Profilku</span>
                </div> */}
            </div>
        )
    }
}


export default connect("userDetail, token")(LsbwPhoto);