import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import InviteStatusOffer from "./InviteStatusOffer.1";
import { connect } from "unistore/react";
import InviteStatusRequest from "./InviteStatusRequest";

class InviteList extends Component {
  render() {
    if (this.props.type == "offer") {
      return (
        <div className="row row-middle">
          <div className="col-md-12 request-item">
            <div className="row">
              <div className="col-md-12">
                  <Link to={{ pathname: "/profileother/tentang/" + this.props.partner_id }}>
                    <img src={this.props.urlPict} />
                  </Link>
                  <Link to={{ pathname: "/profileother/tentang/" + this.props.partner_id }}>
                    <span>
                      <strong>{this.props.name}</strong>
                    </span><br />
                  </Link>
                  <span className="invited-time">Invited at : {this.props.date}</span><br />
                  <label>Status: <strong>{this.props.status}</strong></label>
              {/* </div>
              <div className="col-md-6" style={{paddingTop:"50px"}}> */}
                <InviteStatusOffer id={this.props.id} status={this.props.status} />
              </div>
            </div>
            <hr />
          </div>
        </div>
      );
    } else {
      return (
        <div class="row row-middle">
          <div class="col-md-12 request-item">
            <div className="row">
              <div className="col-md-12">
              <Link to={{ pathname: "/profileother/tentang/" + this.props.users_id }}>
                <img src={this.props.urlPict} />
                <span>
                  <strong>{this.props.name}</strong>
                </span><br />
                <span className="invited-time">Invited at : {this.props.date}</span><br />
                <label>Status: <strong>{this.props.status}</strong></label><br />
                {/* <br /> */}
              </Link>
              {/* </div> */}

              {/* <div className="col-md-6" style={{paddingTop:"50px"}}> */}
              <InviteStatusRequest
                id={this.props.id}
                users_id={this.props.users_id}
                partner_id={this.props.partner_id}
                status={this.props.status}
              />
              </div>
            </div>
            <hr />
          </div>
        </div>
      );
    }
  }
}

export default connect("token")(InviteList);
