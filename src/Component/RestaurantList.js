import React, { Component } from "react";
import { Link } from "react-router-dom";
import MyMap from "../Component/MyMap";
import GeolocationProps from "../Component/GeolocationProps";
import axios from "axios";
import { connect } from "unistore/react";

// https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU&key=YOUR_API_KEY
class RestaurantList extends Component {
    render() {
        const img_src =
            "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" +
            this.props.reference +
            "&key=AIzaSyBnOC2cYnLyaaYXtnd_IEQWZLkqvg0tqoE";
        let dirLink = "https://www.google.co.id/maps/dir/"+this.props.userDetail.latitude+","+this.props.userDetail.longitude+"/"+this.props.name
        return (
            <div class="card card-body">
            <a href={dirLink} target="_blank">
                {/* <div className="image-location"> */}
                {this.props.reference ? 
                <img className="foto-lokasi" src={img_src} /> : 
                <img className="foto-lokasi" src="https://www.consultoriaiso.org/wp-content/uploads/2017/07/ISO-22000-2.jpg" />}
                <div className="deskripsi-lokasi lokasi-nama">
                    {this.props.name}
                    {/* {this.props.rating} */}
                </div>
                <div className="deskripsi-lokasi lokasi-rate">
                    {/* {this.props.name} */}
                    <div><img width="15px" height="15px" src="https://image.flaticon.com/icons/svg/148/148841.svg"/> {this.props.rating} | {this.props.distance} ({this.props.duration}) </div>
                </div>
                {/* </div> */}
                {/* </div> */}

                {/* <div>Rating : {this.props.rating}</div> */}
                {/* <div>Rating : {this.props.rating}</div> */}
                {/* <div>Alamat : {this.props.alamat}</div> */}
                {/* <div>Status : {this.props.status}</div> */}
                {/* <div>Jam Buka : </div> */}
                {/* <div>Telp : </div> */}
                {/* <div>Jarak : {this.props.distance}</div> */}
                {/* <div>Estimasi Waktu : {this.props.duration}</div> */}
                </a>
            </div>
        );
    }
}

export default connect("userDetail")(RestaurantList);
