import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import InviteStatusOffer from "./InviteStatusOffer.1";
import { connect } from "unistore/react";
import InviteStatusRequest from "./InviteStatusRequest";
import GroupList from "./GroupList";
import UserList from "./UserList";

class SearchDetail extends Component {
  render() {
    let data = this.props.data;
    if (this.props.type == "posting") {
      return (
        <div className="row">
            {data.map((item, key) => {
                return (
                    <Link to={"/profileother/post/detail/"+item.id}><div class="post-box">
                      <div class="row">
                        <div class="col-md-1 foto-post">
                          <img
                            width="50px"
                            height="50px"
                            src={item['users.urlPict']}
                            />
                        </div>
                        <div class="col-md-11">
                          <span>
                            <strong>{item['users.name']}</strong>
                          </span>
                          <img
                            class="gambar-post"
                            src={item.urlPict}
                          />
                          <div class="post-text">
                            <p>
                              {" "}
                              {item.posting}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    </Link>
                  );
            })}
        </div>
      );
    } else if(this.props.type == "offer") {
      return (
        <div className="row">
            {data.map((item, key) => {
                return <GroupList name={item.name} place={item.place} date={item.date} id={item.id} />
            })}
        </div>
      );
    } else {
      return (
        <div className="row">
            {data.map((item, key) => {
                return <UserList id={item.id} name={item.name}  urlPict={item.urlPict} />
            })}
        </div>
      );
    }
  }
}

export default connect("token")(SearchDetail);
