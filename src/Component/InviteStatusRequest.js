import React, { Component } from "react";
import axios from "axios";
import { connect } from "unistore/react";
import { withRouter } from "react-router-dom";
import CheckRating from "./CheckRating";
import swal from 'sweetalert2';

class InviteStatusRequest extends Component {
    state = {
        rating: 1,
        comment: "",
    }

    inputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    FinishRequest = (e) => {
        const self = this;
        let token = this.props.token
        //Offer
        axios
            .put("https://api.temenmakan.online/api/user/invite/change/" + e.target.name, { status: "Finish" }, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                // alert("Selamat :D Yuk beri rating ke partner makanmu")
                swal({
                    position: 'top-center',
                    type: 'success',
                    title: 'Selamat :D Yuk beri rating ke partner makanmu',
                    showConfirmButton: false,
                    timer: 1700
                  })
            })
            .catch((err) => {
                console.log(err)
            })
    }

    DeclineRequest = (e) => {
        const self = this;
        let token = this.props.token
        //Offer
        axios
            .put("https://api.temenmakan.online/api/user/invite/change/" + e.target.name, { status: "Declined" }, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                // alert("Yah sayang sekali :(")
                swal("Yah sayang sekali :(")
            })
            .catch((err) => {
                console.log(err)
            })
    }

    AcceptRequest = (e) => {
        const self = this;
        let token = this.props.token
        let id = this.props.users_id 
        // Offer
        axios
            .put("https://api.temenmakan.online/api/user/invite/change/" + e.target.name, { status: "Accepted" }, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                // alert("Asyik, Yuk Chat Temen Makan mu :D");
                swal({
                    position: 'top-center',
                    type: 'success',
                    title: 'Asyik, Yuk Chat Temen Makan mu :D',
                    showConfirmButton: false,
                    timer: 1700
                  })
                self.props.history.push("/chat/"+id );

            })
            .catch((err) => {
                console.log(err)
            })
    }

    SendRating = () => {
        const self = this;
        let token = this.props.token
        axios
            .post("https://api.temenmakan.online/api/users/rating", { invite_id: self.props.id, comment: self.state.comment, rating: self.state.rating }, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                // alert("Terima Kasih")
                swal("Terima Kasih")
            })
            .catch((err) => {
                // alert("Anda telah memberikan Rating :(")
                swal("Anda telah memberikan Rating :(")
            })
    }

    render() {
        if (this.props.status == "Unconfirmed") {
            return (
                <div className="button-respon">
                    {/* {this.props.status} */}
                    <div className="buttons">
                        <button className="form-control" name={this.props.id} onClick={(e) => this.DeclineRequest(e)}
                            className="btn" style={{borderRadius:"20px"}}>Reject</button>
                    </div>
                    <div className="buttons">
                        <button className="form-control" name={this.props.id} id={this.props.partner_id} style={{borderRadius:"20px"}} onClick={(e) => this.AcceptRequest(e)} className="btn btn-success">Accept</button>
                    </div>
                </div>
            )
        } else if (this.props.status == "Accepted") {
            return (
                <div class="button-respon">
                    {/* {this.props.status} */}
                    <button className="form-control" style={{backgroundColor:"#00a896", border:0, borderRadius:"20px", color: "white", height:"40px", width:"100px"}} name={this.props.id} id={this.props.id} onClick={(e) => this.FinishRequest(e)}>Selesai</button>
                </div>
            )
        } else if (this.props.status == "Finish") {
            return (
                <CheckRating id={this.props.id}/>
            )
        } else {
            return (
                <div class="button-respon">
                    {/* {this.props.status} */}
                </div>
            )
        }
    }
}

export default withRouter(connect("token")(InviteStatusRequest));