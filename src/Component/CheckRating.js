import React, { Component } from "react";
import axios from "axios";
import { connect } from "unistore/react";
import swal from "sweetalert2";

class CheckRating extends Component {

    state = {
        ratingData: "Not Found",
        rating: 1,
        comment: "",
    }

    inputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    SendRating = () => {
        const self = this;
        let token = this.props.token
        axios
            .post("https://api.temenmakan.online/api/users/rating", { invite_id: self.props.id, comment: self.state.comment, rating: self.state.rating }, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                // alert("Terima Kasih");
                swal("Terima Kasih")
                this.loadInvite();
            })
            .catch((err) => {
                // alert("Anda telah memberikan Rating :(")
                // swal("Anda telah memberikan Rating :(")
            })
    }

    loadInvite= () =>{
        const self = this;
        let token = this.props.token
        //Offer
        axios
            .get("https://api.temenmakan.online/api/users/check/rating/" + self.props.id, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ ratingData: response.data });

            })
            .catch((err) => {
                console.log(err)
            })
    }

    componentDidMount = () => {
        this.loadInvite();
    }

    render() {
        if (this.state.ratingData == "Not Found") {
            return (
                <div className="request-feedback">
                    {this.props.status}
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Beri rating rekan makan kamu:</label>
                        <select name="rating" class="request-feedback-rating" id="exampleFormControlSelect1" onChange={(e) => this.inputChange(e)}>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select><br />
                        <label for="exampleFormControlTextarea1">Kasih komentar teman-makan kamu:</label>
                        <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" placeholder="Isi ulasan tentang rekan anda" onChange={(e) => this.inputChange(e)} rows="3"></textarea><br />
                        <button type="submit" class="btn btn-info" onClick={this.SendRating}>Kirim</button>
                    </div>
                </div>
            )
        } else {
            var stars;
            if (this.state.ratingData.rating > 1 && this.state.ratingData.rating < 2.5) {
                stars =
                    <div>
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                    </div>
            }
            else if (this.state.ratingData.rating >= 2.5 && this.state.ratingData.rating < 3.5) {
                stars =
                    <div>
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                    </div>
            }
            else if (this.state.ratingData.rating >= 3.5 && this.state.ratingData.rating < 4.5) {
                stars =
                    <div>
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                    </div>
            }
            else if (this.state.ratingData.rating >= 4.5) {
                stars =
                    <div>
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                        <img alt="star" width="1px" height="1px" src="https://image.flaticon.com/icons/svg/148/148841.svg" />
                    </div>
            }
            else {
                stars =
                    <img width="1px" height="1px"
                    //  src="https://image.flaticon.com/icons/svg/149/149222.svg"
                     src="https://image.flaticon.com/icons/svg/148/148841.svg"
                    />
            }
            return (
                <div className="request-feedback stars-small">
                    <strong>{stars} 
                    {/* {this.state.ratingData.rating} */}
                    </strong><br />
                    {this.state.ratingData.comment}
                </div>
            )
        }
    }
}

export default connect("token")(CheckRating);