import React, { Component } from 'react';
import MainRoute from "./Routes/MainRoute";
import { connect } from "unistore/react";
import { Link, withRouter, Redirect } from 'react-router-dom';
import firebase from 'firebase';
import FileUploader from 'react-firebase-file-uploader';

const config = {
    apiKey:
        "AIzaSyBIFIgfpDNa3WN7kz1vNeuKyJgLm_HIS0g",
    authDomain: "temen-makan.firebaseapp.com",
    databaseURL: "https://temen-makan.firebaseio.com",
    storageBucket: "gs://temen-makan.appspot.com"
};
firebase.initializeApp(config);

class App extends Component {
  render() {
    let is_login = this.props.token
    if(!is_login && (this.props.location.pathname != '/' && this.props.location.pathname != '/register')){
      // alert(this.props.token)
      // return <Redirect to={{ pathname: "/" }} />
    }
    return (
      <div>
        <MainRoute/>
      </div>
    );
  }
}

export default connect("token") (withRouter(App));
