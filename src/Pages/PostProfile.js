import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import LsbPhoto from "../Component/LSBPhoto";
import NavbarProfile from "../Component/NavbarProfile";
import Truncate from 'react-truncate';
import axios from "axios";
import firebase from 'firebase';
import FileUploader from 'react-firebase-file-uploader';
import swal from 'sweetalert2';

// const config = {
//   apiKey:
//     "AIzaSyBIFIgfpDNa3WN7kz1vNeuKyJgLm_HIS0g",
//   authDomain: "temen-makan.firebaseapp.com",
//   databaseURL: "https://temen-makan.firebaseio.com",
//   storageBucket: "gs://temen-makan.appspot.com"
// };
// firebase.initializeApp(config);

class Post extends Component {
  state = {
    allposts: [],
    urlPict: "",
    posting: "",
    userID: "",
    buttonEditTampil: true,
    username: '',
    avatar: '',
    isUploading: false,
    progress: 0,
    avatarURL: ''
  };

  inputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    console.log(this.state)
  };

  postPosting = () => {
    console.log("hey", this.state);
    const body = {
      urlPict: this.state.urlPict,
      posting: this.state.posting
    };
    const url = "https://api.temenmakan.online/api/posting/post";
    const auth = "Bearer " + this.props.token
    const header = {
      Authorization: auth
    };

    console.log("body", body);
    axios
      .post(url, body, { headers: header })
      .then(response => {
        // alert("Review kamu telah di-post!");
        swal({
          position: 'top-center',
          type: 'success',
          title: 'Review kamu telah di-post!',
          showConfirmButton: false,
          timer: 1700
        })
        this.setState({
          urlPict: "",
          posting: ""
        });
        this.loadPost();
        console.log("Response review: ", response);
      })
      .catch(err => {
        console.log(err);
      })
  }

  loadPost = () => {
    this.props.getUserDetail(this.props.token);
    let id = this.props.match.params.id;
    if (!id) {
      id = this.props.userDetail.id
    }
    const self = this;
    const url = "https://api.temenmakan.online/api/user/posting/" + id;
    const auth = "Bearer " + this.props.token;
    axios
      .get(url, { headers: { Authorization: auth } })
      .then(response => {
        self.setState({
          allposts: response.data
        });
        console.log("posts", response.data.post);
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount = () => {
    this.loadPost();
  };

  handleChangeUsername = (event) => this.setState({ username: event.target.value });
  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
  handleProgress = (progress) => this.setState({ progress });
  handleUploadError = (error) => {
    this.setState({ isUploading: false });
    console.error(error);
  }
  handleUploadSuccess = (filename) => {
    this.setState({ avatar: filename, progress: 100, isUploading: false });
    firebase.storage().ref('images').child(filename).getDownloadURL().then(url => this.setState({ avatarURL: url, urlPict: url }));
  };

  compareValues = (key, order='asc') => {
    return function(a, b) {
      if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }
  
      const varA = (typeof a[key] === 'string') ?
        a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string') ?
        b[key].toUpperCase() : b[key];
  
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order == 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  render() {
    let id = this.props.match.params.id;
    const buttonEditTampil = this.state.buttonEditTampil;
    if (!id) {
      id = this.props.userDetail.id
    }
    const { allposts } = this.state;
    console.log("allpost di render : ", allposts)
    return (
      <div>
        <NavBar />
        <div className="container-fluid main-content">
          <div className="row main-content-home">
            <LsbPhoto id={id} buttonEditTampil={buttonEditTampil} />
            <div class="col-md-9">
              <div className="profile-right">
                <NavbarProfile />
                <hr />
                <h4>My Post</h4>
                {/* <div class="todo-list post-input text-content" onSubmit={(e) => e.preventDefault()}> */}
                <div class="row">
                  <div class="col-md-12">
                    <form className="form-upload-image text-left">
                      <div className="text-left">
                        <label style={{ backgroundColor: 'steelblue', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor' }}>
                          Pilih file foto
<FileUploader
                            hidden
                            accept="image/*"
                            storageRef={firebase.storage().ref('images')}
                            onUploadStart={this.handleUploadStart}
                            onUploadError={this.handleUploadError}
                            onUploadSuccess={this.handleUploadSuccess}
                            onProgress={this.handleProgress}
                          />
                        </label>
                      </div>
                      {this.state.isUploading &&
                        <progress value={this.state.progress} /> &&
                        // <span>{this.state.progress}</span>&&
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" style={{ width: this.state.progress + "%" }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">Uploading...</div>
                        </div>

                      }


                      {this.state.avatarURL &&
                        <img style={{ height: '200px' }} src=
                          {this.state.avatarURL}
                        />
                      }<br />
                    </form>
                    <input
                      class="form-control"
                      type="text"
                      value={this.state.urlPict}
                      name="urlPict"
                      placeholder="Contoh: 'http://imgur.com/myfoto.jpg'"
                      onChange={(e) => this.inputChange(e)}
                    />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label>Deskripsi Foto Makanan</label>
                    <textarea
                      class="form-control review-posting"
                      type="text"
                      value={this.state.posting}
                      name="posting"
                      placeholder="Review makanan"
                      onChange={(e) => this.inputChange(e)}
                    />
                  </div>
                </div>
                <div class="row post-attribute-button">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 button-makan-apa-right">
                    <button
                      class="button-makan-apa form-control"
                      onClick={() => this.postPosting()}>
                      Post
                      </button>
                  </div>
                </div>
                {/* </div> */}
              </div>
              <div className="profile-right">
                <div class="my-post">
                  <div className="col-md-12"><h4>My Post</h4></div>
                  {allposts.sort(this.compareValues('created_at', 'desc')).map((item, key) => {
                    return (
                      <div class="post-box">
                        <div class="row">
                          <div class="col-md-1 foto-post">
                            <Link to="/profile/tentang"><img
                              width="50px"
                              height="50px"
                              src={item['users.urlPict']}
                            /></Link>
                          </div>
                          <div class="col-md-11">

                            <div className="row deskripsi-post">
                              <div className="col-md-6">
                                <span>
                                  <Link to="/profile/tentang">
                                  <strong>{item['users.name']}</strong>
                                  </Link>
                                </span>
                              </div>
                            </div>
                            <div class="post-text short">
                              <Truncate lines={3}
                              // ellipsis={}
                              >
                                <p>
                                  {item.posting}
                                </p>
                              </Truncate>

                            </div>
                            <Link to={"/post/" + item.id}>
                              <img
                                class="gambar-post"
                                src={item.urlPict}
                              />
                            </Link>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  "userDetail, token, is_login",
  actions
)(withRouter(Post));
