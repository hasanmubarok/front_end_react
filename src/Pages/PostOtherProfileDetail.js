import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import axios from "axios";
import LSBPhotoOtherDetail from "../Component/LSBPhotoOtherDetail";

class PostOtherDetail extends Component {
  state = {
    allposts: [],
    allcomments: [],
    postID:"",
    comment: "",
  };

  inputChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  }

  SendComment = (e) => {        
    const self = this;
    let token = this.props.token
    axios
    .post("https://api.temenmakan.online/api/comment/post",{postID: e.target.name, comment: self.state.comment},{
        headers: { Authorization: "Bearer " + token }
    })
    .then(function(response){
        // alert("Pesan Terkirim")
        self.loadPost();
        self.setState({ comment: ""});
    })
    .catch((err) => {
        console.log(err)
    })  
}

  loadPost = () => {
    this.props.getUserDetail(this.props.token);
    let id = this.props.match.params.id;
    const self = this;
    const url = "https://api.temenmakan.online/api/other/posting/" + id;
    const auth = "Bearer " + this.props.token;
    axios
      .get(url, { headers: { Authorization: auth } })
      .then(response => {
        self.setState({
          allposts: response.data.posts,
          allcomments: response.data.comment
        });
        // console.log("allpost",this.state.allposts)
        self.setState({
          postID : response.data.posts.user_id,
        });
        // console.log("posts", response.data.posts);
        // console.log("comment", response.data.comment);
      })
      .catch(err => {
        console.log(err);
      });
  }
  componentDidMount = () => {
    this.loadPost();
  };

  render() {
    const { allposts, allcomments} = this.state;
    console.log(">>>",allposts)
    console.log("<<<",allcomments)
    if(allposts.hasOwnProperty("user_id")){
        return (
            <div>
        <NavBar />
        <div className="container-fluid main-content">
          <div className="row main-content-home">
            <LSBPhotoOtherDetail userID={allposts.user_id}/>
            <div class="col-md-9">
              <div class="post">
                <h5>Post Details</h5>
                <hr />
                <div class="post-box">
                  <div class="row">
                    <div class="col-md-1 foto-post">
                      <img alt="image-all-post"
                        width="50px"
                        height="50px"
                        src={allposts.user_pict}
                        />
                    </div>
                    <div class="col-md-11">
                      <span>
                        <strong>{allposts.user_name}</strong>
                      </span>
                      <p>
                          {" "}
                          {allposts.posting}
                        </p>
                      <img alt="posting-image"
                        class="gambar-post"
                        src={allposts.urlpict}
                        />
                      <div class="post-text">
                        {/* <hr></hr> */}
                        <span><strong>Menampilkan {allcomments.length} komentar</strong></span>
                        {/* Ini komentar */}
                        {allcomments.map((item, key) => {
                            return (
                                <div class="col-md-12 order command-list">
                              <div class="command-item ">
                                <div class="command-guest ">
                                  <img alt="user_avatar" src={item.user_pict} />
                                </div>
                                <div class="desc-chat ">
                                  <span><strong>{item.user_name}</strong></span>
                                  <div class="chat-text-block ">
                                    <div class="command-text-cover ">
                                      <span>{item.comment}</span>
                                    </div>
                                  </div>
                                  <span class="chat-time">{item.created_at}</span>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                        {/* End looping komentar */}

                        {/* Start input form komentar */}
                        <div class="col-md-12 text-right command-list command-input">
                          <textarea style={{borderRadius:"20px"}} className="form-control" name="comment" value={this.state.comment} placeholder="Isi komentar..." onChange={this.inputChange}/>
                          <button style={{borderRadius:"20px"}} name={allposts.id} className="form-control btn btn-success" onClick={this.SendComment}>Submit</button>
                        </div>
                        {/* End of form Komentar */}
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    );
    }
    else{
        return ""
    }
  }
}

export default connect(
  "userDetail, token, is_login",
  actions
)(withRouter(PostOtherDetail));
