import React, { Component } from "react";
import { Link } from "react-router-dom";
import './Register.css';
import axios from "axios";
import { connect } from "unistore/react";
import { actions } from "../store";
import { withRouter } from 'react-router-dom';
import * as EmailValidator from "email-validator";
import { geolocated } from 'react-geolocated';

class Register extends Component {

    state = {
        longitude: "",
        latitude: "",
        dataCity: [],
        username: "",
        email: "",
        password: "",
        repassword: "",
        name: "",
        gender: "",
        dateofBirth: "",
        urlPict: "",
        address: "",
        currentCityID: "",
        occupation: "",
        descSelf: "",
        likeFood: "",
        status: "Makan Bareng? Quy!",
        wanttoEat: "All",
        passwordNotMatch: false,
        emailNotValid: false
    }

    componentDidMount = () => {
        //Get Loc
        // this.props.coords;
        //Get City
        const self = this;
        axios
            .get("https://api.temenmakan.online/api/city")
            .then(function (response) {
                self.setState({ dataCity: response.data });
                console.log(response.data)
            })
    }

    inputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    checkPasswordMatch(e) {
        if (e.target.value !== this.state.password && e.target.value.length >= this.state.password.length) {
            this.setState({ passwordNotMatch: true })
        } else {
            this.setState({ passwordNotMatch: false })
        }
        // console.log("e.target: ", e.target.value.length)
        // console.log("pass in state: ", this.state.password.length)
    }

    validateEmail(e) {
        console.log("email", e.target.value)
        console.log("validator", EmailValidator.validate(e.target.value))
        if (!EmailValidator.validate(e.target.value)) {
            this.setState({ emailNotValid: true })
        } else {
            this.inputChange(e)
            this.setState({ emailNotValid: false })
        }
    }

    Register = (e) => {
        const self = this;
        axios
            .post("https://api.temenmakan.online/api/user/register", {
                username: self.state.username,
                email: self.state.email,
                password: self.state.password,
                repassword: self.state.repassword,
                name: self.state.name,
                gender: self.state.gender,
                dateofBirth: self.state.dateofBirth,
                currentCityID: 1,
                status: self.state.status,
                wanttoEat: self.state.wanttoEat,
                latitude: self.state.latitude,
                longitude: self.state.longitude
            })
            .then(function (response) {
                self.props.signInHandle(self.state.username, self.state.password, self.state.latitude, self.state.longitude);
                self.props.history.push('/profile/tentang');
            })
            .catch((err) => {
                console.log(err)
            })
    }

    render() {

        let { dataCity } = this.state;
        if (this.props.coords != null && this.props.latitude == "") {
            this.props.getLocation(this.props.coords.longitude, this.props.coords.latitude)
        }

        console.log("latitude1", this.state.latitude)
        console.log("longitude1", this.state.longitude)

        !this.state.longitude && this.props.coords && this.setState({ latitude: this.props.coords.latitude, longitude: this.props.coords.longitude })
        return !this.props.isGeolocationAvailable
            ? <div>Your browser does not support Geolocation</div>
            : !this.props.isGeolocationEnabled
                ? <div>Geolocation is not enabled</div>
                : this.props.coords
                    ?
                    <div className="main-signup">
                        <section className="signup">
                            <div className="container">
                                <div className="signup-content">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="signup-form">
                                                <h2 className="form-title"> Registrasi</h2>
                                                <div className="form-group">
                                                    <label for="name"><i className="zmdi zmdi-account material-icons-name"></i></label>
                                                    <input type="text" name="name" id="name" placeholder="Nama kamu" onChange={(e) => this.inputChange(e)} />
                                                </div>
                                                <div className="form-group">
                                                    <label for="username"><i className="zmdi zmdi-account material-icons-name"></i></label>
                                                    <input type="text" name="username" id="username" placeholder="Username" onChange={(e) => this.inputChange(e)} />
                                                </div>
                                                <div className="form-group">
                                                    <label for="pass"><i className="zmdi zmdi-lock"></i></label>
                                                    <input type="password" name="password" id="pass" placeholder="Password" onChange={(e) => this.inputChange(e)} />
                                                </div>
                                                <div className="form-group">
                                                    <label for="re-pass"><i className="zmdi zmdi-lock-outline"></i></label>
                                                    <input type="password" name="repassword" id="re_pass" placeholder="Ulangi password" onChange={(e) => this.checkPasswordMatch(e)} />
                                                    <small className="text-danger"><i>{(this.state.passwordNotMatch) ? "password tidak sama" : ""}</i></small>
                                                </div>
                                                <div className="form-group">
                                                    <label for="email"><i className="zmdi zmdi-email"></i></label>
                                                    <input type="email" name="email" id="email" placeholder="Email" onChange={(e) => this.validateEmail(e)} />
                                                    <small className="text-danger"><i>{(this.state.emailNotValid) ? "format email belum sesuai!" : ""}</i></small>
                                                </div>
                                                <div className="form-group">
                                                    Tanggal Lahir :
                                <label><i className="zmdi zmdi-calendar-note"></i></label>
                                                    <input type="date" placeholder="dateofBirth" name="dateofBirth" onChange={this.inputChange} />
                                                </div>
                                                <div className="form-group">
                                                    Jenis Kelamin :
                                            <div className="rs-select2 js-select-simple select--no-search">
                                                        <select name="gender" onChange={this.inputChange}>
                                                            <option disabled="disabled" selected="selected">Pilih</option>
                                                            <option value="laki-laki">Laki-laki</option>
                                                            <option value="perempuan">Perempuan</option>
                                                        </select>
                                                        <div className="select-dropdown"></div>
                                                    </div>
                                                </div>
                                                {/* <div className="form-group">
                                            <div className="rs-select2 js-select-simple select--no-search">
                                                <select name="currentCityID" onChange={this.inputChange}>
                                                    <option disabled="disabled" selected="selected">Kota sekarang</option>
                                                    {dataCity.map((item, key) => {
                                                        return <option value={item.id}>{item.name}</option>
                                                    })}
                                                </select>
                                                <div className="select-dropdown"></div>
                                            </div>
                                        </div> */}
                                                <div className="form-group form-button text-center">
                                                    <input type="submit" name="signup" id="signup" className="form-submit" value="Daftar" style={{fontWeight:"bold"}} onClick={this.Register} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div class="signup-image">
                                                <figure><img src="https://i.ibb.co/pR7yWR9/146957-OUND6-B-98.png" alt="sing up image" /></figure>
                                                <div className="center" style={{ textAlign: "center" }}>
                                                    <a href="/" class="signup-image-link"><p>Saya sudah punya akun</p></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    : <div>Getting the location data&hellip; </div>
    }
}

// export default connect("token, is_login, type", actions)(withRouter(Register));
export default connect(
    "token, is_login, type, longitude, latitude",
    actions
)(geolocated({
    positionOptions: {
        enableHighAccuracy: true,
    },
    userDecisionTimeout: 5000,
})(withRouter(Register)))