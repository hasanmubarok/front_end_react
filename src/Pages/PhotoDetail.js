import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import axios from "axios";
import LsbPhoto from "../Component/LSBPhoto";
import swal from 'sweetalert2';

const styleDeletePhotoButton = {
    border:0, 
    borderRadius: "20px",
    backgroundColor: "#00a896",
    color: "white",
    height: "40px",
    width: "80%",
    marginBottom: "30px"
}

class PhotoDetail extends Component{
    state = {
        photo: {}
    };

    componentDidMount = () => {
        let id = this.props.match.params.id;

        const self = this;
        const url = "https://api.temenmakan.online/api/gallery/" + id;
        const auth = "Bearer " + this.props.token;
        axios
            .get(url, { headers: { Authorization: auth } })
            .then(response => {
                self.setState({
                    photo: response.data
                });
                console.log("photonya", response.data);
            })
            .catch(err => {
                console.log(err);
            });
    };

    deletePhoto = () => {
        let id = this.props.match.params.id;
        let token = this.props.token
        axios
        .delete("https://api.temenmakan.online/api/gallery/" + id,
        { headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            // alert("Sukses menghapus foto!")
            swal({
                position: 'top-center',
                type: 'success',
                title: 'Berhasil menghapus foto',
                showConfirmButton: false,
                timer: 1700
              })
        })
    };

    render(){
        const {photo} = this.state;
        return(
            <div>
                <NavBar/>
                <div className="container-fluid main-content">
                    <div className="row main-content-home">
                        <LsbPhoto />
                        <div className="col-md-9 col-xs-8 col-foto-detail">
                            <div className="profile-right" id="foto-background">
                                <div className="row">
                                    <div className="col-md-8" style={{paddingRight:0}}>
                                        <img style={{width:"100%", height:"100%", borderTopLeftRadius: "20px", borderBottomLeftRadius: "20px"}} src={photo.urlGalleryPict} alt="detail photo"/>
                                    </div>
                                    <div className="col-md-4" style={{textAlign:"left"}}>
                                        <div className="row">
                                            <div style={{margin:"20px"}}className="col-md-10">
                                                <p>{photo.caption}</p>
                                            </div>
                                        </div>
                                        <div className="button-detail-photo">
                                            <Link to="/profile/gallery"><button style={styleDeletePhotoButton} onClick={() => this.deletePhoto()}>Delete Photo</button></Link>
                                        </div>
                                        {/* <br/> */}
                                        <div className="button-detail-photo">
                                            <Link to="/profile/gallery"><button style={styleDeletePhotoButton}>Back</button></Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect("userDetail, token, is_login", actions)(withRouter(PhotoDetail));