import React, { Component } from "react";
import NavBar from "../Component/navbar";
import axios from "axios";
import LsbwPhoto from "../Component/LSBWPhoto";
import ListAvailable from "../Component/ListAvailable";
import { connect } from "unistore/react";
import { Link } from "react-router-dom";
import GroupList from "../Component/GroupList";
import NotFound from "./NotFound";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import GeolocationProps from "../Component/GeolocationProps";
import TrialMap from "./TrialMap";
import swal from 'sweetalert2';

class Home extends Component {

    notify = () => toast("Wow so easy !");

    state = {
        userAvailable: [],
        listGroup: [],
        userDetail: {},
        //Sidebar
        userData: "",
        status: "",
        wanttoEat: "",
        //Limit
        limit: 5,
        limitGroup: 5,
        placeKeyword:this.props.userDetail.wanttoeat,
    }

    addLimit = () => {
        this.setState({ limit: this.state.limit + 5 })
    }

    addLimitGroup = () => {
        this.setState({ limitGroup: this.state.limitGroup + 5 })
    }

    inputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    getUserAvailable = () => {
        const self = this;
        let token = this.props.token
        axios
            .get("https://api.temenmakan.online/api/users", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ userAvailable: response.data.users });
                console.log(response.data)
            })
    }

    componentDidMount = () => {
        const self = this;
        let token = this.props.token
        //Get User Status
        axios
            .get("https://api.temenmakan.online/api/user/status", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ userData: response.data });
                self.setState({ status: response.data.status });
            })
        //Get Users Available
        axios
            .get("https://api.temenmakan.online/api/users", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ userAvailable: response.data.users });
                console.log(response.data)
            })
        //Get Group
        axios
            .get("https://api.temenmakan.online/api/user/group", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ listGroup: response.data });
                console.log(response.data)
            })
    }

    componentWillMount = () => {
        this.detail = this.props.userDetail;
        this.setState({
            wanttoEat: this.detail.wanttoeat,
        })
    }

    updateStatus = (e) => {
        const self = this;
        let token = this.props.token
        if (self.state.wanttoEat == "") {
            swal("Makanan tidak boleh kosong!")
        } else {
            //Get Users Available
            axios
                .put("https://api.temenmakan.online/api/user/status", { status: self.state.status, wanttoEat: self.state.wanttoEat }, {
                    headers: { Authorization: "Bearer " + token }
                })
                .then(function (response) {
                    self.getUserAvailable();
                    // alert("Berhasil Update Status")
                    swal({
                        position: 'top-center',
                        type: 'success',
                        title: 'Berhasil memperbarui data',
                        showConfirmButton: false,
                        timer: 1700
                      })
                    self.setState({placeKeyword : self.state.wanttoEat})
                })
        }
    }

    myFunction() {
        var x = document.getElementById("harus-tahu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        let myData=[]
        const makanan = this.state.userData.wanttoEat;
        const { userAvailable, listGroup } = this.state;
        // console.log(">>>>>>>>>>",this.props)
        // console.log("<<<<<<<<<<",this.state)
        if (this.props.is_login) {
            return (
                <div>
                    <NavBar />
                    <div className="container-fluid main-content">
                        <div className="row main-content-home">
                            {/* <LsbwPhoto/> */}
                            <div className="col-md-3 lsbwphoto">
                                <div className="status-makan text-content">
                                    <span>
                                        <h4><strong>{this.props.userDetail.username}</strong></h4>
                                    </span>
                                    {/* <br /> */}
                                    {/* <span>{this.props.userDetail.cityname}</span>
                                    <br />
                                    <span>Indonesia</span> */}
                                    <hr />
                                    <div className="status-available">
                                        <form>
                                            Status:
                                                <select name="status" className="form-control" id="sel1" onChange={this.inputChange}>
                                                <option selected>{this.state.userData.status}</option>
                                                <option>Lagi Pengen Makan Sendiri Aja</option>
                                                <option>Makan Bareng? Quy!</option>
                                            </select>
                                        </form>
                                    </div>
                                    <div className="row status-available">
                                        <div
                                            className="col-md-12"
                                            id="lagi-ingin-makan"
                                        >
                                            {/* <span>Pengen makan : {makanan}<br /> */}
                                            <span>Ingin makan apa?<br />
                                                {/* (Last Value) */}
                                            </span>
                                        </div>
                                        <div
                                            className="col-md-12"
                                            id="option-ingin-makan"
                                        >
                                            <input style={{ borderTopLeftRadius: "20px", borderBottomLeftRadius: "20px", border: "1px", borderColor: "rgba(0,0,0,.2)", borderStyle: "solid", height: "40px", width: "200px" }} name="wanttoEat" type="text" placeholder="Ingin makan apa?" value={this.state.wanttoEat} onChange={this.inputChange} />
                                            <button style={{ backgroundColor: "#00a896", border: 0, borderTopRightRadius: "20px", borderBottomRightRadius: "20px", color: "white", height: "40px", width: "89px" }} onClick={this.updateStatus}>Perbarui</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Sidebar============================================== */}
                            <div className="col-md-9">
                                <div className="todo-list text-content">
                                    <h5 onClick={() => this.myFunction()}> <i class="fas fa-exclamation-circle"></i> Kamu Harus tahu!</h5>
                                    <div id="harus-tahu">
                                        {/* <hr /> */}
                                        <div className="row">
                                            <div className="col-md-9 col-sm-9 col-xs-9 ikon-teman text-left">
                                                <img
                                                    width="40px"
                                                    height="40px"
                                                    src="https://image.flaticon.com/icons/svg/1161/1161388.svg"
                                                />
                                                <h6>
                                                    <strong>Keamanan</strong>
                                                </h6>
                                                <span>
                                                    Bertemu dan makan bareng temen baru? Kami ingin kamu
                                                    selalu aman.
                                                </span>
                                            </div>
                                            <div className="col-md-3 col-sm-3 col-xs-3 button-petunjuk text-center">
                                                <div className="petunjuk">
                                                    <Link to="/petunjuk">
                                                        <button style={{ borderRadius: "20px" }} type="button" className="btn btn-info">
                                                            Petunjuk
                                                            </button>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="restaurant-list">
                                    <div class="container-fluid">
                                        <h5><i class="fas fa-map-marked"></i> REKOMENDASI TEMPAT MAKAN UNTUK KAMU</h5>
                                        <TrialMap keyword={this.state.placeKeyword} />
                                    </div>
                                </div>
                                <div className="container-fluid second-content-home">
                                    <div className="row ">
                                        <div className="col-lg-6 col-md-12 col-sm-12 kolom-makan-left">
                                            <div className="teman-sekitar">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <h5><i class="fas fa-male"></i> TEMAN MAKAN DI SEKITARMU </h5>
                                                        <hr />
                                                    </div>
                                                </div>
                                                {/* ini component makan teman */}
                                                <div style={{ paddingLeft: '25px' }}>
                                                    {userAvailable.length > 0 ? '' : 'Ubah status anda menjadi available'}
                                                </div>
                                                {userAvailable.slice(0, this.state.limit).map((item, key) => {
                                                    return <ListAvailable name={item.username} lng={item.longitude} ltd={item.latitude} gender={item.gender} cuisine={item.wanttoEat} id={item.id} urlPict={item.urlpict} wanttoEat={item.wanttoeat} age={item.age} />
                                                })}
                                                <div className="text-right">
                                                    <Button
                                                        // style={{backgroundColor:"#00a896", border:0, color: "white", borderRadius:"20px", height:"40px", width:"100px", margin:"20px"}}
                                                        outline color="secondary"
                                                        className="btn btn-more"
                                                        onClick={this.addLimit}><i class="fas fa-chevron-circle-down"></i> More</Button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 col-md-12 col-sm-12 rounded kolom-makan-right">
                                            <div className="makan-rame">
                                                <div className="row">
                                                    <div className="col-md-8">
                                                        <h5><i class="fas fa-users"></i> MAKAN RAMEAN SABI NIH!</h5>
                                                    </div>
                                                    <div className="col-md-4" style={{ textAlign: "right" }}>
                                                        <div className="coltext-right button-create-group">
                                                            <Link to="/group"><Button
                                                                outline color="#00a896" className="btn btn-create-group"><i class="fas fa-plus-circle"></i> Bikin Event</Button></Link>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <hr />
                                                    </div>
                                                </div>
                                                {listGroup.slice(0, this.state.limitGroup).map((item, key) => {
                                                    return <GroupList name={item.name} place={item.place} date={item.date} id={item.id} membercount={item.membercount} />
                                                })}
                                                <div className="text-right">
                                                    <Button
                                                        // style={{backgroundColor:"#00a896", border:0, color: "white", borderRadius:"20px", height:"40px", width:"100px", margin:"20px"}}
                                                        outline color="secondary"
                                                        className="btn btn-more"
                                                        onClick={this.addLimitGroup}><i class="fas fa-chevron-circle-down"></i> More</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <ToastContainer />
                <button onClick={this.notify}>Notify</button>
                </div>
            );
        } else {
            return (
                <NotFound />
            )
        }
    }
}

export default connect("is_login, userDetail, token")(Home)

