import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import LsbPhoto from "../Component/LSBPhoto";
import NavbarProfile from "../Component/NavbarProfile";
import axios from "axios";
import NotFound from "./NotFound";
import firebase from "firebase";
import FileUploader from "react-firebase-file-uploader";
import CustomUploadButton from "react-firebase-file-uploader/lib/CustomUploadButton";
import swal from 'sweetalert2';

// const config = {
//     apiKey: "AIzaSyBIFIgfpDNa3WN7kz1vNeuKyJgLm_HIS0g",
//     authDomain: "temen-makan.firebaseapp.com",
//     databaseURL: "https://temen-makan.firebaseio.com",
//     storageBucket: "gs://temen-makan.appspot.com"
// };
// firebase.initializeApp(config);

class Gallery extends Component {
    state = {
        //     allphotos: []
        buttonEditTampil: true
    };

    loadPage = () => {
        const self = this;
        const url = "https://api.temenmakan.online/api/gallery";
        const auth = "Bearer " + this.props.token;
        axios
            .get(url, { headers: { Authorization: auth } })
            .then(response => {
                self.setState({
                    allphotos: response.data.photos
                });
                console.log("photos", response.data.photos);
            })
            .catch(err => {
                console.log(err);
            });
    }

    componentDidMount = () => {
        this.loadPage();
    };

    inputChange = e => {
        this.setState({ [e.target.name]: e.target.value });
        console.log("help", this.state);
    };

    postPhoto = () => {
        let token = this.props.token;
        axios
            .post(
                "https://api.temenmakan.online/api/gallery/post",
                {
                    urlGalleryPict: this.state.avatarURL ? this.state.avatarURL : this.state.urlGalleryPict,
                    caption: this.state.caption
                },
                {
                    headers: { Authorization: "Bearer " + token }
                }
            )
            .then(function (response) {
                // alert("Sukses menambah foto!");
                swal({
                    position: 'top-center',
                    type: 'success',
                    title: 'Berhasil menambah foto',
                    showConfirmButton: false,
                    timer: 1700
                  })
            });
    };

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            allphotos: []
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
        this.loadPage();
    }

    state = {
        username: "",
        avatar: "",
        isUploading: false,
        progress: 0,
        avatarURL: ""
    };
    handleChangeUsername = event =>
        this.setState({ username: event.target.value });
    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    handleProgress = progress => this.setState({ progress });
    handleUploadError = error => {
        this.setState({ isUploading: false });
        console.error(error);
    };
    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref("images")
            .child(filename)
            .getDownloadURL()
            .then(url => this.setState({ avatarURL: url }));
    };

    render() {
        const { allphotos } = this.state;
        const buttonEditTampil = true;
        console.log("helpppp", buttonEditTampil);
        if (this.props.is_login) {
            return (
                <div>
                    <NavBar />
                    <div className="container-fluid main-content">
                        <div className="row main-content-home">
                            <LsbPhoto
                                id={this.props.userDetail.id}
                                buttonEditTampil={buttonEditTampil}
                            />
                            <div className="col-md-9 col-xs-8">
                                <div className="profile-right">
                                    <NavbarProfile />
                                    <hr />
                                    <div className="row galeri ">
                                        <div className="col-md-4 col-xs-8">
                                            <h4>Galeri Foto</h4>
                                        </div>
                                        <div className="col-md-8 col-xs-4 btn-upload text-right">
                                            <button className="btn btn-info" onClick={this.toggle}>
                                                Unggah Foto
                      </button>
                                            <Modal
                                                isOpen={this.state.modal}
                                                toggle={this.toggle}
                                                className={this.props.className}
                                            >
                                                <ModalHeader
                                                    style={{ borderRadius: "20px" }}
                                                    toggle={this.toggle}
                                                >
                                                    <h3>Unggah Foto</h3>
                                                </ModalHeader>
                                                <ModalBody>
                                                    <form className="form-upload-image text-center">
                                                        <label
                                                            style={{
                                                                backgroundColor: "steelblue",
                                                                color: "white",
                                                                padding: 10,
                                                                borderRadius: 4,
                                                                pointer: "cursor"
                                                            }}
                                                        >
                                                            Pilih Foto
                              <FileUploader
                                                                hidden
                                                                accept="image/*"
                                                                storageRef={firebase.storage().ref("images")}
                                                                onUploadStart={this.handleUploadStart}
                                                                onUploadError={this.handleUploadError}
                                                                onUploadSuccess={this.handleUploadSuccess}
                                                                onProgress={this.handleProgress}
                                                            />
                                                        </label><br/>
                                                        {this.state.isUploading && (
                                                            <progress value={this.state.progress}/>&&
                                                            // <span>{this.state.progress}</span>
                                                            <div class="progress">
                                                                <div
                                                                    class="progress-bar progress-bar-striped bg-info progress-bar-animated"
                                                                    role="progressbar"
                                                                    style={{ width: this.state.progress + "%" }}
                                                                    aria-valuenow="50"
                                                                    aria-valuemin="0"
                                                                    aria-valuemax="100"
                                                                >
                                                                    Uploading...
                                </div>
                                                            </div>
                                                        )}

                                                        {this.state.avatarURL && (
                                                            <img
                                                                style={{ height: "200px" }}
                                                                src={this.state.avatarURL}
                                                            />
                                                        )}
                                                        <br />
                                                    </form>
                                                    <form className="form-signin">
                                                        <div
                                                            className="form-group"
                                                            onSubmit={e => e.preventDefault()}
                                                        >
                                                            <label for="urlGalleryPict">URL Foto</label>
                                                            <input
                                                                name="urlGalleryPict"
                                                                type="text"
                                                                className="form-control"
                                                                id="url-foto"
                                                                aria-describedby="url-foto"
                                                                placeholder="Contoh: 'http://imgur.com/myfoto.jpg'"
                                                                onClick={e => this.inputChange(e)}
                                                                value={this.state.avatarURL}
                                                            />
                                                        </div>
                                                        <div className="form-group">
                                                            <label for="caption">Caption</label>
                                                            <input
                                                                name="caption"
                                                                type="text"
                                                                className="form-control"
                                                                id="caption"
                                                                placeholder="Caption foto kamu"
                                                                onChange={e => this.inputChange(e)}
                                                            />
                                                        </div>
                                                    </form>
                                                </ModalBody>
                                                <ModalFooter>
                                                    <button
                                                        type="submit"
                                                        className="btn btn-success button-signin"
                                                        onClick={() => this.postPhoto()}
                                                    >
                                                        Submit
                          </button>
                                                </ModalFooter>
                                            </Modal>
                                        </div>
                                        <hr />
                                        {allphotos.map((item, key) => {
                                            return (
                                                <div className="col-md-4 col-sm-6">
                                                    <div className="foto">
                                                        <Link to={"/profile/gallery/" + item.id}>
                                                            <img src={item.urlGalleryPict} />
                                                        </Link>
                                                        <div className="caption-foto">
                                                            <span>{item.caption}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <NotFound />;
        }
    }
}

export default connect(
    "userDetail, token, is_login",
    actions
)(withRouter(Gallery));
