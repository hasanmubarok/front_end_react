import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import axios from "axios";
import LSBPhoto from "../Component/LSBPhoto";
import { Button } from 'reactstrap';
import swal from 'sweetalert2';

class PostDetail extends Component {
  state = {
    allposts: [],
    allcomments: [],
    postID: "",
    comment: "",
  };

  inputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  SendComment = (e) => {
    let id = e.target.name
    let user_id = e.target.id
    const self = this;
    let token = this.props.token
    axios
      .post("https://api.temenmakan.online/api/comment/post", { postID: id, comment: self.state.comment }, {
        headers: { Authorization: "Bearer " + token }
      })
      .then(function (response) {
        // alert("Pesan Terkirim")
        self.loadPost();
        self.setState({ comment: "" });
         //Send Notif
         axios
         .post("https://api.temenmakan.online/api/user/notifcation", {id_receiver: user_id, id_type:3 , id_object: id},{
             headers: { Authorization: "Bearer " + token }
         })
         .then(function (response) {
             console.log("Notif Data",response.data)
         })
         .catch((err) => {
             console.log(err)
         })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  loadPost = () => {
    this.props.getUserDetail(this.props.token);
    let id = this.props.match.params.id;
    const self = this;
    const url = "https://api.temenmakan.online/api/other/posting/" + id;
    const auth = "Bearer " + this.props.token;
    axios
      .get(url, { headers: { Authorization: auth } })
      .then(response => {
        self.setState({
          allposts: response.data.posts,
          allcomments: response.data.comment
        });
        self.setState({
          postID: response.data.posts.user_id,
        });
        console.log("posts", response.data.posts);
        console.log("comment", response.data.comment);
      })
      .catch(err => {
        console.log(err);
      });
  }

  deletePosting = () => {
    const self = this;
    let id = this.props.match.params.id;
    const url = "https://api.temenmakan.online/api/posting/" + id;
    const auth = "Bearer " + this.props.token;
    axios
      .delete(url, { headers: { Authorization: auth } })
      .then(function(response){
        // alert("Sukses menghapus review!")
        swal({
          position: 'top-center',
          type: 'success',
          title: 'Sukses menghapus review',
          showConfirmButton: false,
          timer: 1700
        })
        self.props.history.push("/profile/post" );

    })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount = () => {
    this.loadPost();
  };

  render() {
    const { allposts, allcomments, postID } = this.state;
    console.log(".....",allcomments)
      return (
        <div>
        <NavBar />
        <div className="container-fluid main-content">
          <div className="row main-content-home">
            <LSBPhoto id={postID} />
            <div class="col-md-9">
              <div class="post">
                <h5>Post Details</h5>
                <hr />
                <div class="post-box">
                  <div className="button-delete-post-box">
                    <Button outline color="secondary" onClick={() => this.deletePosting()}><i class="fa fa-trash-alt"></i></Button>{' '}
                  </div>
                  <div class="row">
                    <div class="col-md-1 foto-post">
                      <Link to="/profile/tentang">
                      <img alt="all-post"
                        width="50px"
                        height="50px"
                        src={allposts.user_pict}
                        />
                      </Link>
                    </div>
                    <div class="col-md-11">
                      <span>
                        <Link to="/profile/tentang">
                        <strong>{allposts.user_name}</strong>
                        </Link>
                      </span>
                      <p>
                        {" "}
                        {allposts.posting}
                      </p>
                      <img alt="post-image"
                        class="gambar-post"
                        src={allposts.urlpict}
                        />
                      <div class="post-text">
                        <span><strong>Menampilkan {allcomments.length} komentar</strong></span>
                        {/* Ini komentar */}
                        {allcomments.map((item, key) => {
                          return (
                            <div class="col-md-12 order command-list">
                              <div class="command-item ">
                                <div class="command-guest ">
                                  <img alt="avatar-user" src={item.user_pict} />
                                </div>
                                <div class="desc-chat ">
                                  <span><strong>{item.user_name}</strong></span>
                                  <div class="chat-text-block ">
                                    <div class="command-text-cover ">
                                      <span>{item.comment}</span>
                                    </div>
                                  </div>
                                  <span class="chat-time">{item.created_at}</span>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                        {/* End looping komentar */}

                        {/* Start input form komentar */}
                        <div class="col-md-12 text-right command-list command-input">
                          <textarea style={{ borderRadius: "20px" }} className="form-control" name="comment" value={this.state.comment} placeholder="Isi komentar..." onChange={this.inputChange} />
                          <button id={allposts.user_id}  style={{ borderRadius: "20px" }} name={allposts.id} className="form-control btn btn-success" onClick={this.SendComment}>Submit</button>
                        </div>
                        {/* End of form Komentar */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  "userDetail, token, is_login",
  actions
)(withRouter(PostDetail));
