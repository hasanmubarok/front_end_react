import React, { Component } from "react";
import "../App.css";
import { Link } from "react-router-dom";

class NotFound extends Component {
    render() {
        return (
            <div className="row">
                <div className="loading text-center">
                    <img src="https://i.ibb.co/FXWRWXM/273036-P5-UPCM-151.png"></img>
                    <Link to="/"><button className="btn btn-success">Back</button></Link>
                </div>
            </div>
        )
    }
}
export default NotFound;