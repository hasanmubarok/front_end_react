import React, {Component} from "react";
import NavBar from "../Component/navbar";
import LsbwPhoto from "../Component/LSBWPhoto";

class FeedBack extends Component{
    render(){
        return(
            <div>
                <NavBar/>
                <div className="container-fluid main-content">
                    <div className="row main-content-home">
                        <LsbwPhoto/>
                        <div className="col-md-9">
                            <div className="request">
                                <div className="row border text-center">
                                    <div className="col-md-6 border button-request">
                                        <button type="button" id="pesan-button" className="btn btn-warning"><strong>Request</strong></button>
                                    </div>
                                    <div className="col-md-6 border button-meetup">
                                        <button type="button" id="pesan-button" className="btn btn-primary"><strong>Meetup List</strong></button>
                                    </div>
                                </div>
                                <div className="row border row-middle">
                                    <div className="col-md-12 border request-item">
                                        <img src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                        <span><strong>Fatur Rohman</strong></span><br/>
                                        <form className="rating">
                                            <label>
                                                <input type="radio" name="stars" value="1" />
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="2" />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="3" />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="4" />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="5" />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                        </form><br/>
                                        <textarea className="rounded" placeholder="Ketik pesan ..."></textarea><br/>
                                        <div className="text-right submit-command">
                                            <input type="button" placeholder="submit" value="submit"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="row border row-middle">
                                    <div className="col-md-12 border request-item">
                                        <img src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                        <span><strong>Fatur Rohman</strong></span><br/>
                                        <form className="rating">
                                            <label>
                                                <input type="radio" name="stars" value="1" disabled />
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="2" disabled />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="3" disabled />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="4" checked />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="stars" value="5" disabled />
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                                <span className="icon">★</span>
                                            </label>
                                        </form><br/>
                                        <div className="command-rated">
                                            <span>Satu bulan lalu</span><br />
                                            <p>Tria orangnya seru banget! Ketemu sekali waktu makan bareng di resto M kemarin,
                                                bisa
                                                banget ngobrolin apa aja! Preferensi makanan kita agak beda tapi dia anaknya
                                                pemaka
                                                segala jadi rekomendasi makanan dari dia macem - macem banget! Gak adan nyesel
                                                deh
                                                makan bareng Tria!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FeedBack;