import React, { Component } from "react";
import NavBar from "../Component/navbar";
import LSBPhotoOther from "../Component/LSBPhotoOther";
import axios from "axios";
import RatingList from "../Component/RatingList";
import { connect } from "unistore/react";
import NavbarProfileOther from "../Component/NavbarProfileOther";

class Referensi extends Component {
    state = {
        ratingData: [],
        otherUserDetail: [],
        totalRating: 0,
    }

    componentDidMount = () => {
        const self = this;
        let token = self.props.token
        //Offer
        axios
            .get("https://api.temenmakan.online/api/users/rating/" + this.props.match.params.id, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ ratingData: response.data });
                self.setState({ type: "request" });
                console.log(response.data)
                let count = 0;
                {
                    self.state.ratingData.map((item) => {
                        self.setState({ totalRating: self.state.totalRating += item.rating });
                        count += 1;
                    })
                }
                self.setState({ totalRating: self.state.totalRating / count });

            })
            .catch((err) => {
                console.log(err)
            })

        const auth = "Bearer " + this.props.token
        axios
            .get("https://api.temenmakan.online/api/user/" + this.props.match.params.id, { headers: { "Authorization": auth } })
            .then((response) => {
                self.setState({
                    otherUserDetail: response.data
                })
            })
            .catch((err) => {
                console.log(err)
            })
    }

    compareValues = (key, order = 'asc') => {
        return function (a, b) {
            if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                // property doesn't exist on either object
                return 0;
            }

            const varA = (typeof a[key] === 'string') ?
                a[key].toUpperCase() : a[key];
            const varB = (typeof b[key] === 'string') ?
                b[key].toUpperCase() : b[key];

            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }
            return (
                (order == 'desc') ? (comparison * -1) : comparison
            );
        };
    }

    render() {
        let id = this.props.match.params.id;
        if (!id) {
            id = this.props.userDetail.id
        }
        let { ratingData, otherUserDetail } = this.state;

        return (
            <div>
                <NavBar />
                <div class="container-fluid main-content">
                    <div class="row main-content-home ">
                        <LSBPhotoOther id={id} />
                        <div class="col-md-9 col-xs-8">
                            <div class="profile-right">
                                <NavbarProfileOther id={id} />
                                <hr />
                                <div class="row ">
                                    <div class="col-md-8 ">
                                        <h4>Referensi</h4>
                                    </div>
                                    <div className="col-md-4">
                                        {/* <span> Rata-rata Rating : {this.state.totalRating}</span> */}
                                    </div>
                                </div>
                                <div class="row  referensi">
                                    <div class="col-md-12 col-list-rating">
                                        {ratingData.sort(this.compareValues('invite.created_at', 'desc')).map((item, key) => {
                                            return <RatingList name={item['users.name']} rating={item.rating} comment={item.comment} id={item.users_id} urlPict={item['users.urlPict']} created_at={item['invite.created_at']} />
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect("token, userDetail")(Referensi);