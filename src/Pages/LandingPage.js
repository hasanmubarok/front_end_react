import React, { Component } from "react";
import { Link, withRouter, Redirect } from "react-router-dom";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { connect } from "unistore/react";
import { actions } from "../store";
import "../App.css";
import 'react-toastify/dist/ReactToastify.css';
import { geolocated } from 'react-geolocated';

class LandingPage extends Component {

    state = {
        longitude: "",
        latitude: "",
        username: "",
        password: ""
    };

    componentDidMount = () => {
        this.props.coords && alert(this.props.coords.latitude)
    }

    inputChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        console.log("uname", this.state.username)
        console.log("pass", this.state.password)
        console.log("latitude", this.state.latitude)
        console.log("longitude", this.state.longitude)
        if (this.props.token) {
            return <Redirect to={{ pathname: "/home" }} />;
        }
        if (this.props.coords != null && this.props.latitude == "") {
            this.props.getLocation(this.props.coords.longitude, this.props.coords.latitude)
        }

        !this.state.longitude && this.props.coords && this.setState({ latitude: this.props.coords.latitude, longitude: this.props.coords.longitude })
        return !this.props.isGeolocationAvailable
            ? <div>Your browser does not support Geolocation</div>
            : !this.props.isGeolocationEnabled
                ? <div>Geolocation is not enabled</div>
                : this.props.coords
                    ?
                    <div>
                        <nav class="navbar navbar-expand-lg navbar-light" style={{ width: '100%', zIndex: '1', backgroundColor: '#00a896', fontFamily: 'roboto', fontWeight: '500', fontSize: '18px' }}>
                            <Link to="#" class="navbar-brand mr-auto">
                                <img src="https://i.ibb.co/TLgGVYw/garpu-putih-4.png" width="auto" height="50px" alt="Logo Temen-Makan" />
                            </Link>
                            <button
                                class="navbar-toggler ml-auto"
                                type="button"
                                data-toggle="collapse"
                                data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent"
                                aria-expanded="false"
                                aria-label="Toggle navigation"
                            >
                                <span class="navbar-toggler-icon" />
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link text-white" onClick={this.toggle} href="#">Masuk</a>
                                    </li>
                                    <li class="nav-item">
                                        <Link class="nav-link text-white" to="/aboutus">Tentang Kami</Link>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div className="landing-page-main">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6 show-image">
                                        <img alt="landing-poster" src="https://i.ibb.co/6gF8B8b/146957-OUND6-B-98.png" />
                                    </div>
                                    <div className="col-md-6 landing-login">
                                        <div>
                                            <h1>Makan Sendirian ?</h1>
                                            <h2>Mana Seru ...</h2>
                                            <Link to="/register"><Button className="button-register" color="success">
                                                Ayo Daftar Teman Makan
                                    </Button></Link>
                                            <Modal
                                                isOpen={this.state.modal}
                                                toggle={this.toggle}
                                                className={this.props.className}
                                            >
                                                <ModalHeader toggle={this.toggle}><h3>Login</h3></ModalHeader>
                                                <ModalBody>
                                                    <form className="form-signin">
                                                        <div className="form-group" onSubmit={(e) => e.preventDefault()}>
                                                            <label for="exampleInputEmail1">Username</label>
                                                            <input name="username" type="username" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                                                placeholder="Username" onChange={(e) => this.inputChange(e)} />
                                                        </div>
                                                        <div className="form-group">
                                                            <label for="exampleInputPassword1">Password</label>
                                                            <input name="password" type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" onChange={(e) => this.inputChange(e)} />
                                                        </div>
                                                    </form>
                                                </ModalBody>
                                                <ModalFooter>
                                                    <button type="submit" className="btn btn-success button-signin" onClick={() => this.props.signInHandle(this.state.username, this.state.password, this.state.latitude, this.state.longitude)}>Submit</button>
                                                </ModalFooter>
                                            </Modal>
                                        </div>
                                    </div>
                                </div>
                                <div className="row welcome-title">
                                    <div className="col-md-12">
                                        <h1>Welcome to Teman Makan</h1>
                                    </div>
                                </div>
                                <div className="row welcome-title">
                                    <div className="col-md-3">
                                        <img alt="icon-landing" src="https://i.ibb.co/VJs77ks/7-Friends.png" />
                                        <br />
                                        <span>Bertemu banyak teman makan</span>
                                    </div>
                                    <div className="col-md-3">
                                        <img alt="icon-landing" src="https://i.ibb.co/kKHz7qc/126254-OR1-XLO-162.png" />
                                        <br />
                                        <span>Menemukan teman dekat</span>
                                    </div>
                                    <div className="col-md-3">
                                        <img alt="icon-landing" src="https://i.ibb.co/BZkXNJ3/263633-P4-T2-QR-417.png" />
                                        <br />
                                        <span>Menemukan jodoh</span>
                                    </div>
                                    <div className="col-md-3">
                                        <img alt="icon-landing" src="https://image.flaticon.com/icons/svg/135/135637.svg" />
                                        <br />
                                        <span>Rekomendasi makanan kamu</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="page-footer font-small teal pt-4">
                            <div class="container-fluid text-center text-md-left">
                                <div class="row">
                                    <div class="col-md-6 mt-md-0 mt-3">
                                        <h5 class="text-uppercase font-weight-bold">Mengapa TemenMakan?</h5>
                                        <p>TemenMakan dibuat untuk orang-orang yang menginginkan suasana makan yang berbeda. Kami percaya bahwa suasana makan anda sangat menentukan kualitas kegiatan makan anda. </p>
                                    </div>
                                    <hr class="clearfix w-100 d-md-none pb-3" />
                                    <div class="col-md-6 mb-md-0 mb-3">
                                        <h5 class="text-uppercase font-weight-bold">Segera bergabung!</h5>
                                        <p>Dengan bergabung dengan temenmakan, anda menyelamatkan orang-orang yang kebingungan mencari partner makan!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="https://mdbootstrap.com/education/bootstrap/"> Alphatech Academy</a>
                            </div>
                        </footer>
                    </div>
                    : <div>Getting the location data&hellip; </div>
    }
}

export default connect(
    "token, is_login, type, longitude, latitude",
    actions
)(geolocated({
    positionOptions: {
        enableHighAccuracy: true,
    },
    userDecisionTimeout: 5000,
})(withRouter(LandingPage)))