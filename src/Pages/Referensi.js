import React, { Component } from "react";
import NavBar from "../Component/navbar";
import LsbPhoto from "../Component/LSBPhoto";
import axios from "axios";
import RatingList from "../Component/RatingList";
import NavbarProfile from "../Component/NavbarProfile";
import {connect} from "unistore/react";

class Referensi extends Component {
    state = {
        ratingData: [],
        totalRating: 0,
        buttonEditTampil : true
    }

    componentDidMount = () => {
        const self = this;
        let token = self.props.token
        let id = this.props.match.params.id;
        if(!id){
        id = this.props.userDetail.id
        }
        //Offer
        axios
            .get("https://api.temenmakan.online/api/users/rating/" + id, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ ratingData: response.data });
                self.setState({ type: "request" });
                console.log(response.data)
                let count = 0;
                {
                    self.state.ratingData.map((item) => {
                        self.setState({ totalRating: self.state.totalRating += item.rating });
                        count += 1;
                    })
                }
                self.setState({ totalRating: self.state.totalRating / count });

            })
            .catch((err) => {
                console.log(err)
            })
    }

    compareValues = (key, order='asc') => {
        return function(a, b) {
          if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // property doesn't exist on either object
            return 0;
          }
      
          const varA = (typeof a[key] === 'string') ?
            a[key].toUpperCase() : a[key];
          const varB = (typeof b[key] === 'string') ?
            b[key].toUpperCase() : b[key];
      
          let comparison = 0;
          if (varA > varB) {
            comparison = 1;
          } else if (varA < varB) {
            comparison = -1;
          }
          return (
            (order == 'desc') ? (comparison * -1) : comparison
          );
        };
    }

    render() {
        let id = this.props.match.params.id;
        const buttonEditTampil = this.state.buttonEditTampil;
        if(!id){
        id = this.props.userDetail.id
        }
        let { ratingData } = this.state;
        console.log("helppppppppp",ratingData)
        return (
            <div>
                <NavBar />
                <div class="container-fluid main-content">
                    <div class="row main-content-home ">
                        <LsbPhoto id={id} buttonEditTampil={buttonEditTampil}/>
                        <div class="col-md-9 col-xs-8">
                            <div class="profile-right">
                                <NavbarProfile />
                                <hr/>
                                <div class="row  referensi">
                                    <h4>Referensi</h4>
                                    {/* Button filter referensi */}
                                    {/* <div class="col-md-12 filter-referensi">
                                        <div class="row text-center">
                                            <div class="col-md-2 button-first">
                                                <button className="btn btn-primary">Semua</button>
                                            </div>
                                            <div class="col-md-5">
                                                <button className="btn btn-success">Mau makan lagi / Positif (10)</button>
                                            </div>
                                            <div class="col-md-5">
                                                <button className="btn btn-danger">Gak mau makan bareng lagi / Negative (0)</button>
                                            </div>
                                        </div>
                                    </div> */}
                                    {/* Enf of button filter */}
                                    <div class="col-md-12 col-list-rating">
                                        {ratingData.sort(this.compareValues('invite.created_at', 'desc')).map((item, key) => {
                                            return <RatingList name={item['users.name']} id={item.users_id} rating={item.rating} comment={item.comment} urlPict={item['users.urlPict']} created_at={item['invite.created_at']} />
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default (connect("token,userDetail") (Referensi));