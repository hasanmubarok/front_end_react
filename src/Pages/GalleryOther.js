import React, {Component} from "react";
import NavBar from "../Component/navbar";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import LSBPhotoOther from "../Component/LSBPhotoOther";
import NavbarProfileOther from "../Component/NavbarProfileOther"
import axios from "axios";
import NotFound from "./NotFound";

class GalleryOther extends Component{
    state = {
        otherUserDetail: [],
        allphotos: []
      };

    componentDidMount = () => {
    this.props.getUserDetail(this.props.token);

    const self = this;
    const url = "https://api.temenmakan.online/api/user/photo/"+this.props.match.params.id;
    const auth = "Bearer " + this.props.token;
    axios
        .get(url, { headers: { Authorization: auth } })
        .then(response => {
        self.setState({
            allphotos: response.data
        });
        console.log("photos", response.data.userID);
        })
        .catch(err => {
        console.log(err);
        });
    axios
        .get("https://api.temenmakan.online/api/user/" + this.props.match.params.id, { headers: { "Authorization": auth } })
        .then((response) => {
            self.setState({
                otherUserDetail: response.data
            })
        })
        .catch((err) => {
            console.log(err)
        })
    };

    render(){
        let id = this.props.match.params.id;
        console.log("idnya",this.props.match.params.id)
        if(!id){
        id = this.props.otherUserDetail.id
        }
        const { allphotos, otherUserDetail } = this.state;
        if (this.props.is_login) {
            return (
                <div>
                    <NavBar/>
                    <div className="container-fluid main-content">
                        <div className="row main-content-home">
                            <LSBPhotoOther id={id}/>
                        <div className="col-md-9 col-xs-8">
                            <div className="profile-right">
                                <NavbarProfileOther id={id}/>
                                    <hr/>
                                <div className="row galeri ">
                                    <div className="col-md-12 col-xs-8">
                                        <h4>Galeri Foto</h4>
                                    </div>
                                        {allphotos.map((item,key) => {
                                            return (
                                                <div className="col-md-4 col-sm-6">
                                                    <div className="foto ">
                                                        <Link to={"/profileother/gallery/detail/"+item.id}>
                                                        <img style={{border:0}} src={item.urlGalleryPict} />
                                                        </Link>
                                                        <div className="caption-foto">
                                                            {item.caption}
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <NotFound />
            )
        }
    }
}

export default connect("userDetail, token, is_login", actions)(withRouter(GalleryOther));