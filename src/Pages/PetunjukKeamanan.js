import React, { Component } from "react";
import NavBar from "../Component/navbar";
import LsbPhoto from "../Component/LSBPhoto";
import "../App.css";

class PetunjukKeamanan extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <div className="container-fluid main-content">
          <div class="row main-content-home border">
            <LsbPhoto />
            <div class="col-md-9 col-xs-8">
              <div class="profile-right">
                <div class="row petunjuk-keselamatan">
                  <div class=" col-md-12 border">
                    <div class="title-petunjuk">
                      <h2>Petunjuk Keamanan</h2>
                    </div>
                    <h5>
                      <strong>Mencari TEMENMAKAN dengan Aman</strong>
                    </h5>
                    <p>
                      TEMENMAKAN menyatukan orang-orang. Setiap harinya,
                      komunitas kami terus berkembang. <br />
                      Dengan begitu banyak orang di TEMENMAKAN, keamanan
                      pengguna adalah prioritas. Kami memahami bahwa bertemu
                      dengan seseorang untuk pertama kalinya, adalah hal yang
                      menarik dan mengasyikkan. Namun, keselamatan Anda sangat
                      penting, ada langkah keamanan tertentu yang harus Anda
                      ikuti saat berinteraksi dengan para TEMENMAKAN - baik
                      online maupun offline. <br />
                      Kami meminta Anda untuk membaca tips dan informasi di
                      bawah ini, dan sangat mendesak Anda untuk mengikuti
                      pedoman ini demi keselamatan pribadi Anda.
                    </p>
                    <h5>
                      <strong>Perilaku Online</strong>
                    </h5>
                    <span>
                      <i class="fas fa-check-circle" /> Lindungi Informasi
                      Pribadi Anda
                      <p>
                        Jangan pernah memberikan informasi pribadi, seperti:
                        informasi pribadi, nomor kartu kredit atau informasi
                        bank, atau kantor atau alamat rumah Anda kepada
                        orang-orang yang tidak Anda kenal atau tidak temui
                        secara pribadi. <br />
                        Catatan: TEMENMAKAN tidak akan pernah mengirimi Anda
                        email yang meminta informasi nama pengguna dan kata
                        sandi Anda.
                      </p>
                    </span>
                    <span>
                      <i class="fas fa-check-circle" /> Kenali Orang Lain
                      <p>
                        Jaga komunikasi Anda terbatas pada platform TEMENMAKAN
                        dan cari tahu lebih baik mengenai partner TEMENMAKAN
                        lewat aplikasi sebelum menemui mereka secara langsung.
                        Kami menyediakan fitur untuk mengecek reputasi dari
                        partner TEMENMAKAN Anda dari partner-partner mereka
                        terdahulu.
                      </p>
                    </span>
                    <span>
                      <i class="fas fa-check-circle" /> Jadilah Web Wise
                      <p>
                        Abaikan ajakan makan atau chat dari pengguna yang
                        mencurigakan. Lakukan pembicaraan hanya lewat aplikasi
                        chat dari TEMENMAKAN, jangan memberikan informasi
                        pribadi seperti nomor telepon atau email pribadi Anda
                        kepada TEMENMAKAN, terutama yang belum Anda kenal baik
                        secara offline.
                      </p>
                    </span>
                    <span>
                      <i class="fas fa-check-circle" /> Perilaku Offline
                      <p>
                        Pertemuan pertama merupakan hal yang menarik, tetapi
                        selalu lakukan tindakan pencegahan dan ikuti panduan ini
                        untuk membantu Anda tetap aman.
                      </p>
                    </span>
                    <span>
                      <i class="fas fa-check-circle" /> Bertemulah di Tempat
                      Umum yang Ramai
                      <p>
                        Kami menghimbau Anda untuk merencanakan pertemuan dengan partner
                        TEMENMAKAN di tempat umum yang ramai - bukan di lokasi
                        pribadi atau terpencil, dan jangan pernah di rumah atau
                        apartemen pribadi Anda. Usahakan untuk memilih restoran
                        atau tempat makan yang memiliki sistem untuk membayar
                        langsung setelah memesan untuk menghindari hal-hal yang
                        tidak diinginkan.
                      </p>
                    </span>
                    <span>
                      <i class="fas fa-check-circle" /> Stay Sober
                      <p>
                        Konsumsi alkohol dan / atau obat lain dapat merusak
                        penilaian Anda dan berpotensi membahayakan Anda. Sangat
                        penting untuk menjaga pikiran yang jernih dan
                        menghindari hal-hal yang dapat membahayakan Anda.
                      </p>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PetunjukKeamanan;
