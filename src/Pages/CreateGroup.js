import React, { Component } from "react";
import NavBar from "../Component/navbar";
import LsbPhoto from "../Component/LSBPhoto";
import { Link, withRouter } from 'react-router-dom';
import { connect } from "unistore/react";
import { actions } from "../store";
import axios from "axios";
import swal from 'sweetalert2';

class CreateGroup extends Component {
    state = {
        dataCity:[],
        city_id : 1,
        date : "",
        name : "",
        place : "",
        description : ""
    }

    inputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    };

    componentDidMount = () => {
        const self = this;
        axios
        .get("https://api.temenmakan.online/api/city")
        .then(function(response){
            self.setState({ dataCity: response.data});
            console.log(response.data)
        })
    }

    createGroup = () => {
        let token = this.props.token
        // alert(token)
        // alert(this.state.city_id)
        // alert(this.state.date)
        // alert(this.state.name)
        // alert(this.state.place)
        // alert(this.state.description)
        axios
        .post("https://api.temenmakan.online/api/user/group", {
            city_id : this.state.city_id,
            date : this.state.date,
            name : this.state.name,
            place : this.state.place,
            description : this.state.description,
        }, { headers: { Authorization: "Bearer " + token }
            })
        .then(function (response) {
            // alert("Sukses membuat event!")
            swal({
                position: 'top-center',
                type: 'success',
                title: 'Sukses membuat event!',
                showConfirmButton: false,
                timer: 1700
              })
        })
        .catch((err) => {
            console.log(err)
            // alert("Mohon mengisi informasi event dengan lengkap")
            swal("Mohon mengisi informasi event dengan lengkap")
        }) 
    };

    render() {
        console.log("coba", this.props.userDetail.id)
        let {dataCity} = this.state;
        return (
            <div>
                <NavBar />
                <div class="container-fluid main-content">
                    <div class="row main-content-home">
                        <LsbPhoto />
                        <div class="col-md-9 col-xs-8">
                            <div class="profile-right">
                                <div class="row update-profile">
                                    <div class=" col-md-12 title-update-profile rounded-top text-center">
                                        <h2>Create Group</h2>
                                        <hr className="green-hr"/>
                                    </div>
                                    
                                    <div class="col-md-6 form-update-profile">
                                        <span>Nama Group</span><br />
                                        <input name="name" className="form-control background-input" type="text" placeholder="Nama Group" onChange={this.inputChange}/><br />
                                        <span>Tanggal Acara Group</span><br />
                                        <input name="date" className="form-control background-input" type="date" onChange={this.inputChange}/><br />
                                    </div>
                                    <div class="col-md-6 form-update-profile">
                                        <span>Lokasi Acara</span><br />
                                        <input name="place" className="form-control background-input" type="text" placeholder="Lokasi" onChange={this.inputChange}/><br />
                                        <span>Kota Acara</span><br />
                                        <select name="city_id" className="form-control" onChange={this.inputChange}>
                                            <option disabled="disabled" selected="selected">Kota sekarang</option>
                                            {dataCity.map((item, key) => {
                                                return <option value={item.id}>{item.name}</option>
                                            })}
                                        </select>
                                        <br />
                                    </div>
                                    <div class="col-md-12 form-update-profile">
                                        <span>Deskripsi Acara</span><br />
                                        <textarea name="description" className="form-control" placeholder="Ceritakan sedikit mengenai acara ini" onChange={this.inputChange} ></textarea>
                                    </div>
                                    <div class="col-md-6 form-update-profile">
                                    </div>
                                    <div class="col-md-6 form-update-profile text-right">
                                        <Link to="/home"><button>Kembali</button></Link>
                                        <Link to="/home"><button onClick={this.createGroup}>Submit</button></Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// export default UpdateProfile;
export default connect("userDetail, token, is_login", actions)(withRouter(CreateGroup));
