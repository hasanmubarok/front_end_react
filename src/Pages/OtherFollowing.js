import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter } from 'react-router-dom';
import { connect } from "unistore/react";
import { actions } from "../store";
// import { Link } from "react-router-dom";
import LSBPhotoOther from "../Component/LSBPhotoOther";
import OtherListFollowing from "../Component/OtherListFollowing";
import NavbarProfileOther from "../Component/NavbarProfileOther";
import {Link} from "react-router-dom";
import axios from "axios";

class Following extends Component {
    state = {
        otherUserDetail: []
    }

    componentDidMount = () => {
        this.props.getUserDetail(this.props.token)
        let id = this.props.match.params.id

        const self = this;
        const url = "https://api.temenmakan.online/api/user/" + id
        const auth = "Bearer " + this.props.token
        axios
            .get(url, { headers: { "Authorization": auth } })
            .then((response) => {
                self.setState({
                    otherUserDetail: response.data
                })
            })
            .catch((err) => {
                console.log(err)
            })
    };

    render() {
        const { otherUserDetail } = this.state;
        let id = this.props.match.params.id;
        if(!id){
        id = this.props.userDetail.id
        }
        return (
            <div>
                <NavBar />
                <div className="container-fluid main-content">
                    <div className="row main-content-home">
                        <LSBPhotoOther/>
                        <div className="col-md-9 col-xs-8">
                            <div className="profile-right">
                                <NavbarProfileOther id={id}/>
                                <OtherListFollowing userDetail={this.props.userDetail} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// export default Profile;
export default connect("userDetail, token, is_login", actions)(withRouter(Following));