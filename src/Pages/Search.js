import React, { Component } from "react";
import NavBar from "../Component/navbar";
import axios from "axios";
import LsbwPhoto from "../Component/LSBWPhoto";
import ListAvailable from "../Component/ListAvailable";
import { connect } from "unistore/react";
import { Link } from "react-router-dom";
import GroupList from "../Component/GroupList";
import NotFound from "./NotFound";
import MemberList from "../Component/MemberList";
import UserList from "../Component/UserList";
// import SearchDetail from "../Component/SearchDetail";

class Search extends Component {

    state = {
        search : this.props.match.params.query,
        datauser : [],
        datagroup : [],
        datapost : []
    }

    componentDidMount = () => {
        const self = this;
        axios
        .get("https://api.temenmakan.online/api/user/search?query="+self.state.search)
        .then(function(response){
            self.setState({ datauser: response.data});
            console.log("users",response.data)
        })
        .catch((err) => {
            console.log(err)
        });

        axios
        .get("https://api.temenmakan.online/api/group/search?query="+self.state.search)
        .then(function(response){
            self.setState({ datagroup: response.data});
            console.log("groups",response.data)
        })
        .catch((err) => {
            console.log(err)
        });

        axios
        .get("https://api.temenmakan.online/api/posting/search?query="+self.state.search)
        .then(function(response){
            self.setState({ datapost: response.data});
            console.log("posts",response.data)
        })
        .catch((err) => {
            console.log(err)
        }) 
    }

    render() {
        const { datauser,datagroup,datapost }= this.state;
        console.log("help search",this.state)
        return (
            <div>
                <NavBar />
                <div className="container-fluid main-content">
                    <div className="row main-content-home">
                        <LsbwPhoto />
                        <div className="col-md-9">
                            <div
                                className="todo-list text-content"
                                style={{ textAlign: "center", borderRadius:"20px"}}
                            >
                                <h5>Hasil Pencarian</h5>
                                <hr />
                                {/* RESULT QUERY SEARCH USER */}
                                <div style={{textAlign:"left", paddingLeft:"22px"}}>
                                    <h6>Orang : </h6>
                                </div>
                                <div className="row">
                                    {datauser.length<1 ? "<No User Found>" : "" }
                                    {datauser.map((item, key) => {
                                        return (
                                            <div className="col-md-6  participant-list">
                                                <div className="row participant-item border" style={{borderRadius:"20px"}}>
                                                    <div className="col-md-1 avatar-participant" style={{textAlign:"left"}}>
                                                        <Link to={{ pathname: "/profileother/tentang/" + item.id }}>
                                                            <img style={{borderRadius:"50%", width:"50px", height:"50px"}} src={item.urlPict} />
                                                        </Link>
                                                    </div>
                                                    <div className="col-md-11 desc-participant" style={{textAlign:"left"}}>
                                                        <Link to={{ pathname: "/profileother/tentang/" + item.id }}>
                                                            <span><strong>{item.name}</strong></span><br />
                                                        </Link>
                                                        <span>{item.gender}, 21 tahun.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                                <hr/>
                                {/* RESULT QUERY SEARCH MABAR */}
                                <div style={{textAlign:"left", paddingLeft:"22px"}}>
                                    <h6>Acara : </h6>
                                </div>
                                <div className="row">
                                    {datagroup.length<1 ? "<No Group Found>" : "" }
                                    {datagroup.map((item, key) => {
                                        return (
                                            <div id="group-section" className="row border" style={{borderRadius:"20px"}}>
                                                <div className="col-md-8 col-sm-8">
                                                    <div className="row">
                                                        <div className="group-detail">
                                                            <div className="span-group-makan">
                                                                <h3 style={{color:"#00a896"}}>{item.name}</h3>
                                                                <br />
                                                                <span>{item.place}</span>
                                                                <br />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 col-sm-4 ikon-teman text-right button-teman">
                                                        <Link to={{pathname: '/group/' + item.id}}>
                                                    <button type="button" className="btn" style={{borderRadius: "20px"}}>Lihat Detail
                                                    </button>
                                                    </Link>
                                                </div>
                                                <div className="row" style={{margin: 0, width: "100%"}}>
                                                    <div style={{textAlign: "left"}} className="col-md-2">
                                                        <img width="30px" height="30px" src="https://image.flaticon.com/icons/svg/138/138773.svg"/>
                                                    </div>
                                                    <div style={{paddingLeft:0, margin:"auto"}} className="col-md-7">
                                                        <span>{item.date}</span>
                                                    </div>
                                                    <div style={{margin: "auto", textAlign:"right"}}className="col-md-3">
                                                        <span>
                                                            2 <i class="fas fa-users" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                                <hr/>
                                {/* RESULT QUERY SEARCH USER */}
                                <div style={{textAlign:"left", paddingLeft:"22px"}}>
                                    <h6>Post : </h6>
                                </div>
                                <div className="row">
                                    {datapost.length<1 ? "<No Post Found>" : "" }
                                    {datapost.map((item, key) => {
                                        return (
                                            <Link to={"/profileother/post/detail/"+item.id}><div class="post-box border" style={{borderRadius:"20px"}}>
                                            <div class="row">
                                                <div class="col-md-1 foto-post" style={{textAlign:"left"}}>
                                                <img
                                                    width="50px"
                                                    height="50px"
                                                    src={item['users.urlPict']}
                                                    />
                                                </div>
                                                <div class="col-md-11" style={{textAlign:"left"}}>
                                                <span>
                                                    <strong>{item['users.name']}</strong>
                                                </span>
                                                <img
                                                    class="gambar-post"
                                                    src={item.urlPict}
                                                />
                                                <div class="post-text">
                                                    <p>
                                                    {" "}
                                                    {item.posting}
                                                    </p>
                                                </div>
                                                </div>
                                            </div>
                                            </div>
                                            </Link>
                                        );
                                    })}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect("is_login, userDetail, token")(Search);
