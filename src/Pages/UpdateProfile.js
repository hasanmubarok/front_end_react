import React, { Component } from "react";
import NavBar from "../Component/navbar";
import LsbPhoto from "../Component/LSBPhoto";
import { Link, withRouter } from 'react-router-dom';
import { connect } from "unistore/react";
import { actions } from "../store";
import axios from "axios";
import swal from 'sweetalert2';

class UpdateProfile extends Component {
    state = {
        dataCity: [],
        name: '',
        cityname: '',
        address: '',
        likeFood: '',
        urlPict: '',
        occupation: '',
        descSelf: '',
        buttonEditTampil: false,
        username: '',
        avatar: '',
        isUploading: false,
        progress: 0,
        avatarURL: ''
    }

    componentDidMount = () => {
        const self = this;
        axios
            .get("https://api.temenmakan.online/api/city")
            .then(function (response) {
                self.setState({ dataCity: response.data });
                console.log(response.data)
            })
    }

    componentWillMount = () => {
        let id = this.props.userDetail.id;
        this.detail = this.props.userDetail;

        this.setState({
            name: this.detail.username,
            cityname: this.detail.cityname,
            address: this.detail.address,
            likeFood: this.detail.wanttoeat,
            urlPict: this.detail.urlpict,
            occupation: this.detail.occupation,
            descSelf: this.detail.descself
        });

        console.log("detail", this.props.item);
    };

    inputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    };

    updateProfile = () => {
        let token = this.props.token
        axios
            .patch("https://api.temenmakan.online/api/user/me", {
                name: this.state.name,
                currentCityID: this.state.currentCityID,
                address: this.state.address,
                likeFood: this.state.likeFood,
                urlPict: this.state.urlPict,
                occupation: this.state.occupation,
                descSelf: this.state.descSelf
            }, {
                    headers: { Authorization: "Bearer " + token }
                })
            .then(function (response) {
                // alert("Sukses memperbarui profile!")
                swal({
                    position: 'top-center',
                    type: 'success',
                    title: 'Sukses memperbarui profile!',
                    showConfirmButton: false,
                    timer: 1700
                  })

            })
    };

    // handleChangeUsername = (event) => this.setState({ username: event.target.value });
    // handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    // handleProgress = (progress) => this.setState({ progress });
    // handleUploadError = (error) => {
    //     this.setState({ isUploading: false });
    //     console.error(error);
    // }
    // handleUploadSuccess = (filename) => {
    //     this.setState({ avatar: filename, progress: 100, isUploading: false });
    //     firebase.storage().ref('images').child(filename).getDownloadURL().then(url => this.setState({ avatarURL: url, urlPict: url }));
    // };

    render() {
        let { dataCity } = this.state;
        const buttonEditTampil = this.state.buttonEditTampil;
        console.log("coba", this.props.userDetail)
        return (
            <div>
                <NavBar />
                <div class="container-fluid main-content">
                    <div class="row main-content-home">
                        <LsbPhoto buttonEditTampil={buttonEditTampil} />
                        <div class="col-md-9 col-xs-8">
                            <div class="profile-right">
                                <div class="row update-profile">
                                    <div class=" col-md-12 title-update-profile rounded-top text-center">
                                        <h2>Perbarui Profil</h2>
                                    </div>
                                    <hr />
                                    <div class="col-md-6 form-update-profile" onSubmit={(e) => e.preventDefault()}>
                                        <span>Nama</span><br />
                                        <input className="form-control" name="name" type="text" placeholder="nama" value={this.state.name} onChange={(e) => this.inputChange(e)} /><br />
                                        <span>Asal</span><br />
                                        <input className="form-control" name="address" type="text" placeholder="alamat asal" value={this.state.address} onChange={(e) => this.inputChange(e)} /><br />
                                        <span>Makanan Favorit</span><br />
                                        <input className="form-control" name="likeFood" type="text" placeholder="makanan favorit" value={this.state.likeFood} onChange={(e) => this.inputChange(e)} /><br />
                                    </div>
                                    <div class="col-md-6 form-update-profile" onSubmit={(e) => e.preventDefault()}>
                                        <span>Pekerjaan</span><br />
                                        <input className="form-control" name="occupation" type="text" placeholder="pekerjaan" value={this.state.occupation} onChange={(e) => this.inputChange(e)} /><br />
                                        <span>Foto Profile</span><br />
                                        {/* <form className="form-upload-image text-center">
                                            <div className="text-left">
                                                <label style={{ backgroundColor: 'steelblue', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor' }}>
                                                    Pilih file foto
<FileUploader
                                                        hidden
                                                        accept="image/*"
                                                        storageRef={firebase.storage().ref('images')}
                                                        onUploadStart={this.handleUploadStart}
                                                        onUploadError={this.handleUploadError}
                                                        onUploadSuccess={this.handleUploadSuccess}
                                                        onProgress={this.handleProgress}
                                                    />
                                                </label>
                                            </div>
                                            {this.state.isUploading &&
                                                <progress value={this.state.progress} /> &&
                                                // <span>{this.state.progress}</span>&&
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" style={{ width: this.state.progress + "%" }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">Uploading...</div>
                                                </div>

                                            }


                                            {this.state.avatarURL &&
                                                <img style={{ height: '200px' }} src=
                                                    {this.state.avatarURL}
                                                />
                                            }<br />
                                        </form> */}
                                        <input className="form-control" name="urlPict" type="text" placeholder="foto profile" value={this.state.urlPict} onChange={(e) => this.inputChange(e)} /><br />
                                        {/* <span>Kota Tinggal</span><br />
                            <div className="form-group">
                                    <select name="currentCityID" className="form-control" onChange={this.inputChange}>
                                        <option disabled="disabled" selected="selected">{this.state.cityname}</option>
                                        {dataCity.map((item, key) => {
                                            return <option value={item.id}>{item.name}</option>
                                        })}
                                    </select>
                                    <div className="select-dropdown"></div>
                            </div> */}
                                        {/* <span>Tanggal Lahir</span><br />
                            <input name="" type="text" placeholder="tanggal lahir" value={this.props.userDetail.dateofbirth} onChange={(e) => this.inputChange(e)}/><br /> */}
                                    </div>
                                    <div class="col-md-12 form-update-profile" onSubmit={(e) => e.preventDefault()}>
                                        {/* <span>Kota Saat Ini</span><br />
                            <input name="currentCityID" type="text" placeholder="text" value={this.props.userDetail.cityname} onChange={(e) => this.inputChange(e)}/><br /> */}
                                        <span>Deskripsi Diri</span><br />
                                        <textarea className="form-control" name="descSelf" placeholder="ceritakan sedikit mengenai dirimu disini" value={this.state.descSelf} onChange={(e) => this.inputChange(e)}></textarea>
                                    </div>
                                    <div class="col-md-6 form-update-profile">
                                    </div>
                                    <div class="col-md-6 form-update-profile text-right">
                                        <Link to="/profile/tentang"><button style={{ cursor: "pointer" }}>Kembali</button></Link>
                                        <Link to="/profile/tentang" onClick={() => this.updateProfile()}><button style={{ cursor: "pointer" }}>Submit</button></Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// export default UpdateProfile;
export default connect("userDetail, token, is_login", actions)(withRouter(UpdateProfile));
