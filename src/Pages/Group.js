import React, { Component } from "react";
import NavBar from "../Component/navbar";
import LsbwPhoto from "../Component/LSBWPhoto";
import axios from "axios";
import { connect } from "unistore/react";
import MemberList from "../Component/MemberList";
import ChatList from "../Component/ChatList";
import NotFound from "./NotFound";

class Group extends Component {

    state = {
        chatData: [],
        message: "",
        dataGroup: [],
        dataMember: [],
        autoScroll:true,
    }

    inputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    loadChat = () => {
        const self = this;
        let token = this.props.token
        //Get Chat
        axios
        .get("https://api.temenmakan.online/api/user/group/chat/" + this.props.match.params.id, {
            headers: { Authorization: "Bearer " + token }
        })
        .then(function (response) {
            self.setState({ chatData: response.data });
            console.log(response.data)
        })
        .catch((err) => {
            console.log(err)
        })
    }

    tick() {
        this.setState({
        time: new Date().toLocaleString()
        });
        this.loadChat();
        this.scrollToBottom();
    }

    componentDidMount = () => {
        const self = this;
        let token = this.props.token
        //Autorefresh 
        this.intervalID = setInterval(
            () => this.tick(),
                1000
        );
        //Get Users Available
        axios
        .get("https://api.temenmakan.online/api/user/group/" + this.props.match.params.id, {
            headers: { Authorization: "Bearer " + token }
        })
        .then(function (response) {
            self.setState({ dataGroup: response.data });
            axios
                .get("https://api.temenmakan.online/api/user/member/" + self.state.dataGroup.id, {
                    headers: { Authorization: "Bearer " + token }
                })
                .then(function (response) {
                    self.setState({ dataMember: response.data });
                    console.log(response.data)
                })
        })
        this.loadChat();
    }

    SendChat = () => {
        const self = this;
        let token = this.props.token
        axios
            .post("https://api.temenmakan.online/api/user/group/chat/" + this.props.match.params.id, { comment: self.state.message }, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                // alert("Pesan Terkirim")
                self.setState({ message: ""});
            })
            .catch((err) => {
                console.log(err)
            })
    }

    JoinRequest = (e) => {
        const self = this;
        let token = this.props.token
        axios
            .post("https://api.temenmakan.online/api/user/member/" + this.props.match.params.id, {}, {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                alert("berhasil")
            })
            .catch((err) => {
                console.log(err)
            })
    }

    scrollToBottom() {
        if(this.state.autoScroll) {
            const scrollHeight = this.messageList.scrollHeight;
            const height = this.messageList.clientHeight;
            const maxScrollTop = scrollHeight - height;
            this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
            this.setState({autoScroll:false})
        }
    }
      

    render() {

        const { dataMember, chatData } = this.state;
        if (this.props.is_login) {
            return (
                <div>
                    <NavBar />
                    <div className="container-fluid main-content">
                        <div className="row main-content-home">
                            <LsbwPhoto />
                            <div className="col-md-9">
                                <div className="group">
                                    <div className="row">
                                        <div className="col-md-10 group-title ">
                                            <h2>{this.state.dataGroup.name}</h2>
                                            <span>
                                                <img alt="flat-icon" width="20px" height="20px" src="https://image.flaticon.com/icons/svg/123/123403.svg" /> {this.state.dataGroup.place}, {this.state.dataGroup['city.name']}
                                            </span><br />
                                            <span><img alt="flat-icon" width="20px" height="20px" src="https://image.flaticon.com/icons/svg/138/138773.svg" /> {this.state.dataGroup.date}</span><br />
                                            <br />
                                            <span>
                                                <strong>Deskripsi Event</strong><br />
                                                {this.state.dataGroup.description}
                                            </span><br />
                                            <br />
                                            <span className="invited-time">created by {this.state.dataGroup['users.name']} at {this.state.dataGroup.created_at}</span><br />
                                        </div>
                                        <div className="col-md-2 group-button ">
                                            <button style={{ borderRadius: "20px" }} className="form-control" onClick={this.JoinRequest}>Join</button>
                                        </div>
                                    </div>
                                    <div className="row participant">
                                        <h4>Participants</h4>
                                        {dataMember.map((item, key) => {
                                            return <MemberList id={item.users_id} name={item['users_name']} gender={item['users_gender']} age={item['users_age']} urlPict={item['users_urlPict']} admin={this.state.dataGroup.creator_id} />
                                        })}
                                    </div>
                                    <div className="row participant chat-group-participant">
                                        <h4>Group Conversation</h4>
                                        <hr />
                                        <div className="scrollbar" id="style-2" ref={(div) => {this.messageList = div;}}>
                                            <div className="force-overflow">
                                                {chatData.map((item, key) => {
                                                    return <ChatList urlPict={item['users.urlPict']} sender={item['users.name']} message={item.comment} time={item.created_at} id={this.props.match.params.userID} sender_id={item.sender_id} />
                                                })}
                                            </div>
                                        </div>
                                        <div className="col-md-12 order">
                                            <div className="conversation-item ">
                                                <br />

                                                <label for="comment">Kirim Pesan:</label><br />
                                                <textarea name="message" className="form-control" onChange={this.inputChange} value={this.state.message}></textarea><br />
                                                <button className="btn btn-info" onClick={this.SendChat}>Kirim</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <NotFound />
            )
        }

    }
}

export default connect("is_login, token")(Group);