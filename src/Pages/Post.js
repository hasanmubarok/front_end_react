import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import LsbwPhoto from "../Component/LSBWPhoto";
import Truncate from 'react-truncate';
import axios from "axios";
import firebase from 'firebase';
import FileUploader from 'react-firebase-file-uploader';
import swal from 'sweetalert2';

// const config = {
//   apiKey:
//     "AIzaSyBIFIgfpDNa3WN7kz1vNeuKyJgLm_HIS0g",
//   authDomain: "temen-makan.firebaseapp.com",
//   databaseURL: "https://temen-makan.firebaseio.com",
//   storageBucket: "gs://temen-makan.appspot.com"
// };
// firebase.initializeApp(config);

class Post extends Component {
  state = {
    allposts: [],
    urlPict: "",
    posting: "",
    username: '',
    avatar: '',
    isUploading: false,
    progress: 0,
    avatarURL: ''
  };


  inputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    console.log(this.state)
  };

  postPosting = () => {
    console.log("hey", this.state);
    const body = {
      urlPict: this.state.urlPict,
      posting: this.state.posting
    };
    const url = "https://api.temenmakan.online/api/posting/post";
    const auth = "Bearer " + this.props.token
    const header = {
      Authorization: auth
    };

    console.log("body", body);
    axios
      .post(url, body, { headers: header })
      .then(response => {
        // alert("Review kamu telah di-post!");
        swal({
          position: 'top-center',
          type: 'success',
          title: 'Review kamu telah di-post!',
          showConfirmButton: false,
          timer: 1700
        })
        
        this.setState({
          urlPict: "",
          posting: ""
        });
        console.log("Response review: ", response);
        this.getData();
      })
      .catch(err => {
        console.log(err);
      })
  }

  getData = () => {
    this.props.getUserDetail(this.props.token);
    let id = this.props.match.params.id;
    if (!id) {
      id = this.props.userDetail.id
    }

    const self = this;
    const url = "https://api.temenmakan.online/api/other/posting";
    const auth = "Bearer " + this.props.token;
    axios
      .get(url, { headers: { Authorization: auth } })
      .then(response => {
        self.setState({
          allposts: response.data.post
        });
        console.log("posts", response.data.post);
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount = () => {
    this.getData();
  };

  // state = {

  // };
  handleChangeUsername = (event) => this.setState({ username: event.target.value });
  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
  handleProgress = (progress) => this.setState({ progress });
  handleUploadError = (error) => {
    this.setState({ isUploading: false });
    console.error(error);
  }
  handleUploadSuccess = (filename) => {
    this.setState({ avatar: filename, progress: 100, isUploading: false });
    firebase.storage().ref('images').child(filename).getDownloadURL().then(url => this.setState({ avatarURL: url , urlPict : url}));
  };


  render() {
    const { allposts, pictures } = this.state;
    console.log("pictures di render : ", pictures)
    return (
      <div>
        <NavBar />
        <div className="container-fluid main-content">
          <div className="row main-content-home">
            <LsbwPhoto />
            <div class="col-md-9">
              <div class="todo-list post-input text-content" onSubmit={(e) => e.preventDefault()}>
                <h5>KAMU MAKAN APA?</h5>
                <hr />
                <div class="row">
                  <div class="col-md-12 input-post-baru">
                    <label>Input Foto</label>
                    <form className="form-upload-image text-center">
                    <div className="text-left">
                      <label style={{ backgroundColor: 'steelblue', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor' }}>
                        Pilih file foto
<FileUploader
                          hidden
                          accept="image/*"
                          storageRef={firebase.storage().ref('images')}
                          onUploadStart={this.handleUploadStart}
                          onUploadError={this.handleUploadError}
                          onUploadSuccess={this.handleUploadSuccess}
                          onProgress={this.handleProgress}
                        />
                      </label>
                      </div>
                      {this.state.isUploading &&
                        <progress value={this.state.progress}/>&&
                        // <span>{this.state.progress}</span>&&
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" style={{ width: this.state.progress + "%" }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">Uploading...</div>
                        </div>

                      }


                      {this.state.avatarURL &&
                        <img style={{ height: '200px' }} src=
                          {this.state.avatarURL}
                        />
                      }<br />
                    </form>
                    <input
                      class="form-control"
                      type="text"
                      // value={this.state.urlPict}
                      value={this.state.avatarURL}
                      name="urlPict"
                      placeholder="Contoh: 'http://imgur.com/myfoto.jpg'"
                      onChange={(e) => this.inputChange(e)}
                    />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 input-post-baru">
                    <label>Deskripsi Foto Makanan</label>
                    <textarea
                      class="form-control review-posting"
                      type="text"
                      value={this.state.posting}
                      name="posting"
                      placeholder="Review makanan"
                      onChange={(e) => this.inputChange(e)}
                    />
                  </div>
                </div>
                <div class="row post-attribute-button">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 button-makan-apa-right">
                    <button
                      class="button-makan-apa"
                      onClick={() => this.postPosting()}>
                      Post
                    </button>
                  </div>
                </div>
              </div>
              <div class="post">
                <h5>TEMEN-TEMEN MAKAN APA?</h5>
                <hr />
                {allposts.map((item, key) => {
                  return (
                    <div class="post-box">
                      <div class="row">
                        <div class="col-md-1 foto-post">
                          <img
                            width="50px"
                            height="50px"
                            src={item.user_pict}
                          />
                        </div>
                        <div class="col-md-11">
                          <span>
                            <strong>{item.user_name}</strong>
                          </span>
                          <div class="post-text short">
                            <Truncate lines={3}
                            // ellipsis={}
                            >
                              <p>
                                {item.posting}
                              </p>
                            </Truncate>

                          </div>
                          <Link to={"/post/all/" + item.id}>
                            <img
                              class="gambar-post"
                              src={item.urlpict}
                            />
                          </Link>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  "userDetail, token, is_login",
  actions
)(withRouter(Post));
