import React, {Component} from "react";
import NavBar from "../Component/navbar";
import LSBPhotoOther from "../Component/LSBPhotoOther";
import "../App.css";
import axios from "axios";
import ChatList from "../Component/ChatList";
import {connect} from "unistore/react";
import { Link } from "react-router-dom";
import NotFound from "./NotFound";

class Chat extends Component{

    state = {
        chatData: [],
        message: "",
        otherUserDetail: [],
        autoScroll:true,
    }   

    inputChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    componentDidMount = () => {
        //Autorefresh 
        this.intervalID = setInterval(
            () => this.tick(),
                1000
        );

        const self = this;
        let token = this.props.token
        axios
        .get("https://api.temenmakan.online/api/user/chat/"+this.props.match.params.userID, {
            headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            self.setState({ chatData: response.data});
            console.log(response.data)
        })
        .catch((err) => {
            console.log(err)
        });

        const url = "https://api.temenmakan.online/api/user/" +this.props.match.params.userID
        const auth = "Bearer " + this.props.token
        axios
            .get(url, { headers: { "Authorization": auth } })
            .then((response) => {
                self.setState({
                    otherUserDetail: response.data
                })
            })
            .catch((err) => {
                console.log(err)
            });

    }

    SendChat = () => {        
        const self = this;
        let token = this.props.token
        axios
        .post("https://api.temenmakan.online/api/user/chat/"+this.props.match.params.userID,{message: self.state.message},{
            headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            // alert("Pesan Terkirim")
            self.setState({ message: ""});
            //Send Notif
            axios
            .post("https://api.temenmakan.online/api/user/notifcation", {id_receiver: self.props.match.params.userID, id_type:2 },{
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                console.log("Notif Data",response.data)
            })
            .catch((err) => {
                console.log(err)
            })
        })
        .catch((err) => {
            console.log(err)
        })  
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }
    
    tick() {
        this.setState({
        time: new Date().toLocaleString()
        });

        const self = this;
        let token = this.props.token
        axios
        .get("https://api.temenmakan.online/api/user/chat/"+this.props.match.params.userID, {
            headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            self.setState({ chatData: response.data});
        })
        .catch((err) => {
            console.log(err)
        }) 
        this.scrollToBottom();
    }

    scrollToBottom() {
        if(this.state.autoScroll) {
            const scrollHeight = this.messageList.scrollHeight;
            const height = this.messageList.clientHeight;
            const maxScrollTop = scrollHeight - height;
            this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
            this.setState({autoScroll:false})
        }
    }
      

    render(){

        let {chatData} = this.state;
        const { otherUserDetail } = this.state;
        if (this.props.is_login) {
            return(
                <div>
                    <NavBar/>
                    <div className="container-fluid main-content">
                        <div className="row main-content-home">
                            <div className="col-md-3 col-xs-4 profile-left">
                                <div className="status-makan text-content">
                                    <div className="text-center profil-kiri">
                                        <img src={otherUserDetail.urlpict} />
                                        <h4>{otherUserDetail.username}</h4>
                                        <span>{otherUserDetail.cityname}</span><br />
                                        <span>Indonesia</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <div className="group">
                                    <div className="row participant">
                                        <div className="col-md-12 top-chat ">
                                            <Link to={"/profileother/tentang/" + otherUserDetail.id}><img id="button-back" src="https://image.flaticon.com/icons/svg/109/109618.svg" /></Link>
                                            <h5>Chat</h5>
                                            {/* <h4>Conversation</h4> */}
                                            <hr/>
                                            <div className="scrollbar" id="style-2" ref={(div) => {this.messageList = div;}}>
                                                <div className="force-overflow">
                                                    {chatData.map((item, key) => {
                                                        return <ChatList urlPict={item['users.urlPict']} sender={item['users.name']} message={item.message} time={item.created_at} id={this.props.match.params.userID} sender_id={item.sender_id}/>
                                                    })}
                                                </div>
                                            </div>
                                            <div className="chat-send-item">
                                                <label for="comment">Kirim Pesan:</label><br/>
                                                <textarea name="message" className="form-control review-posting" onChange={this.inputChange} value={this.state.message}></textarea><br/>
                                                <button className="form-control" style={{backgroundColor: "#00a896", borderRadius:"20px", border: 0, color: "white", height: "40px", width: "90px"}} onClick={this.SendChat}>Kirim</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <NotFound />
            )
        }
        
        }
    }
export default connect("is_login, token") (Chat);

