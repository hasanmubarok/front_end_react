import React, {Component} from "react";
import {Link} from "react-router-dom";
import "../App.css";

class AboutUs extends Component{
    render(){
        return(
            <div>
                <nav class="navbar navbar-expand-lg navbar-light" style={{ width: '100%', zIndex: '1', backgroundColor: '#00a896', fontFamily: 'roboto', fontWeight: '500', fontSize: '18px' }}>
                    <Link to="/" class="navbar-brand mr-auto">
                        <img src="https://i.ibb.co/TLgGVYw/garpu-putih-4.png" width="auto" height="50px" alt="Logo Temen-Makan" />
                    </Link>
                    <button
                    class="navbar-toggler ml-auto"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span class="navbar-toggler-icon" />
                </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            {/* <li class="nav-item">
                                <a class="nav-link text-white" onClick={this.toggle} href="#">Masuk</a>
                            </li> */}
                            <li class="nav-item">
                                <Link class="nav-link text-white" to="/">Kembali</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div className="about-us-body">
                    <h6>Tentang Kami</h6>
                    <p>TEMENMAKAN adalah tempat berkumpulnya orang-orang yang ingin makan bersama. Kami menawarkan sensasi makan yang berbeda dengan mempertemukan anda dengan orang-orang yang memiliki kesukaan yang sama. Segera bergabung dan bertemulah dengan orang-orang yang akan meramaikan suasana makan anda!</p>
                </div>
            </div>
        )
    }
}

export default AboutUs;