import React, {Component} from "react";
import MyMap from "../Component/MyMap";
import GeolocationProps from "../Component/GeolocationProps";
import axios from "axios";
import { connect } from "unistore/react";
import RestaurantList from "../Component/RestaurantList";

class TrialMap extends Component{
  
  state = {
      restaurantData:[],
      distanceData:[],
      distanceDurationData:[{}],
      keyword:this.props.keyword,
  }

  componentWillReceiveProps(nextProps){
    this.setState({keyword: nextProps.keyword });
    this.getPlace();
  }

  componentDidMount =  () => {
    this.getPlace()
  }

  getPlace = () =>{
    let keyword=this.state.keyword;
    let longitude = this.props.longitude
    let latitude =  this.props.latitude
    axios.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+latitude+','+longitude+'&radius=25000&type=restaurant&keyword='+keyword+'&key=AIzaSyBnOC2cYnLyaaYXtnd_IEQWZLkqvg0tqoE')
    .then(response => {
      // this.setState({restaurantData:response.data.results})
      // console.log("get Distance",response.data.results[0].geometry.location.lng)       
      return response.data.results
    }).then(async arrayRestaurant=>{
      let result = await arrayRestaurant.map(async (item)=>{
        let data = await this.getDistance(item.geometry.location.lat,item.geometry.location.lng,latitude,longitude)
        // console.log(JSON.stringify(data))
        return data
      })
      Promise.all(result).then(result => this.setState({distanceDurationData:result, restaurantData:arrayRestaurant}))
    })
  }

getDistance= async (lat,lng,latitude,longitude) => {
  let distance = 0;
  let duration = 0;
  let apiLat = lat
  let apiLng = lng
  let data = await axios.get('https://maps.googleapis.com/maps/api/distancematrix/json?origins='+latitude+','+longitude+'&destinations='+apiLat+','+apiLng+'&key=AIzaSyBnOC2cYnLyaaYXtnd_IEQWZLkqvg0tqoE')
  .then(response => {
    // console.log(response.data)
     distance=response.data.rows[0].elements[0].distance.text
     duration=response.data.rows[0].elements[0].duration.text
    //  console.log({distance,duration})
     return {distance,duration}
    })
    .then(data=>{
      return data
    })
    .catch((err) => {
      console.log(err)
    })
  // console.log(data)
  return data
}

  render() {
    const {restaurantData} = this.state
    console.log(this.state.distanceDurationData)
    return (
      <div class="d-flex flex-row flex-nowrap bungkus-lokasimakan" id="style-2">
          { this.state.distanceDurationData!==undefined ? restaurantData.map((item,key)=>
              <RestaurantList 
                    name={item.name} 
                    rating={item.rating} 
                    alamat={item.vicinity} 
                    status={item.opening_hours && item.opening_hours['open_now']} 
                    reference={item.photos && item.photos['0'].photo_reference} 
                    distance={this.state.distanceDurationData[key].distance} 
                    duration={this.state.distanceDurationData[key].duration}
                      />
          ): null}

      {/* <div>
        <GeolocationProps />
      </div> */}
      </div>
    );
  }
}

export default connect('token, userDetail,longitude,latitude') (TrialMap);