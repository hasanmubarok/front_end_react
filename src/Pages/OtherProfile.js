import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter, Redirect, Link } from 'react-router-dom';
import { connect } from "unistore/react";
import { actions } from "../store";
import NavbarProfileOther from "../Component/NavbarProfileOther";
import LSBPhotoOther from "../Component/LSBPhotoOther";
import axios from "axios";
import NotFound from "./NotFound";

class OtherProfile extends Component {

    state = {
        ratingData: [],
        otherUserDetail: [],
        jarak: 0
    }

    componentDidMount = () => {
        this.props.getUserDetail(this.props.token)
        let id = this.props.match.params.id

        const self = this;
        const url = "https://api.temenmakan.online/api/user/" + id
        const auth = "Bearer " + this.props.token
        axios
            .get("https://api.temenmakan.online/api/users/rating/" + id, {
                headers: { Authorization: auth }
            })
            .then(function (response) {
                self.setState({ ratingData: response.data });
                self.setState({ type: "request" });
                console.log(response.data)
            })
            .catch((err) => {
                console.log(err)
            })
            
        axios
            .get(url, { headers: { "Authorization": auth } })
            .then((response) => {
                self.setState({
                    otherUserDetail: response.data
                })

                axios
                .get('https://maps.googleapis.com/maps/api/distancematrix/json?origins='+this.props.userDetail['latitude']+','+this.props.userDetail['longitude']+'&destinations='+this.state.otherUserDetail.latitude+','+this.state.otherUserDetail.longitude+'&key=AIzaSyBnOC2cYnLyaaYXtnd_IEQWZLkqvg0tqoE')
                .then(function(response){
                    self.setState({ jarak: response.data.rows[0].elements[0].distance.text })
                })
                .catch((err) => {
                    console.log(err)
                })
                    })
            .catch((err) => {
                console.log(err)
            })
    };

    render() {
        let id = this.props.match.params.id;
        if(!id){
            id = this.props.userDetail.id
        }
        const { ratingData, otherUserDetail } = this.state;
        const userDetail = this.props.userDetail;

        console.log(">>>>>", this.props.userDetail['longitude'] + "xxx" + this.props.userDetail['longitude'] + "xxx" + otherUserDetail.latitude + "xxx" + otherUserDetail.longitude)
        // Default Profile if User is new
        var occupation;
        if (otherUserDetail.occupation==="" || otherUserDetail.occupation==="None") {
            occupation = "[Data Pekerjaan Belum Ada]"
        } else {
            occupation = otherUserDetail.occupation
        }
        var address;
        if (otherUserDetail.address==="" || otherUserDetail.address===null) {
            address = "[Data Alamat Asal Belum Ada]"
        } else {
            address = "Dari " + otherUserDetail.address
        }
        var descself;
        if (otherUserDetail.descself==="" || otherUserDetail.descself===null) {
            descself = "[Data Diri User Belum Ada]"
        } else {
            descself = otherUserDetail.descself
        }

        if (this.props.is_login) {
            if (otherUserDetail.username !== userDetail.username) {
                return (
                    <div>
                        <NavBar />
                        <div className="container-fluid main-content">
                            <div className="row main-content-home ">
                                <LSBPhotoOther id={id} jarak={this.state.jarak}/>
                                <div className="col-md-9 col-xs-8">
                                    <div className="profile-right">
                                        <NavbarProfileOther id={id}/>
                                        <hr/>
                                        <div className="row  tinjauan">
                                            <h4>Tinjauan</h4>
                                            <div className="col-md-6 ">
                                                <span><img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/1081/1081947.svg"/> {ratingData.length} Referensi</span><br />
                                                <br/>
                                                <span><img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/196/196131.svg"/> {otherUserDetail.age}, {otherUserDetail.gender}</span><br />
                                                <br/>
                                                <span><img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/265/265683.svg"/> Member sejak {otherUserDetail.membersince}</span><br />
                                                <br/>
                                            </div>
                                            <div className="col-md-6 ">
                                                <span><img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/387/387569.svg"/> {occupation}</span><br />
                                                <br/>
                                                <span><img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/189/189060.svg"/> {address}</span><br />
                                            </div>
                                        </div>
                                        <hr/>
                                        <div className="row  tentangku">
                                            <h4>Tentangku</h4>
                                            <p>{descself}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
            else {
                return (
                    <Redirect to={{ pathname: "/profile/tentang" }} />
                )
            }
        } else {
            return (
                <NotFound />
            )
        }
    }
}

export default connect("userDetail, token, is_login", actions)(withRouter(OtherProfile));