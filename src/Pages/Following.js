import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter } from 'react-router-dom';
import { connect } from "unistore/react";
import { actions } from "../store";
// import { Link } from "react-router-dom";
import LsbPhoto from "../Component/LSBPhoto";
import ListFollowing from "../Component/ListFollowing";
import NavbarProfile from "../Component/NavbarProfile";
import {Link} from "react-router-dom";

class Following extends Component {
    state={
        buttonEditTampil : true
    }
    render() {
        const buttonEditTampil = this.state.buttonEditTampil;
        return (
            <div>
                <NavBar />
                <div className="container-fluid main-content">
                    <div className="row main-content-home">
                        <LsbPhoto buttonEditTampil={buttonEditTampil}/>
                        <div className="col-md-9 col-xs-8">
                            <div className="profile-right">
                                <NavbarProfile/>
                                {/* <Tentangku userDetail={this.props.userDetail} /> */}
                                <ListFollowing userDetail={this.props.userDetail} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// export default Profile;
export default connect("userDetail, token, is_login", actions)(withRouter(Following));