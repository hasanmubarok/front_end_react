import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import axios from "axios";
import LSBPhotoOtherDetail from "../Component/LSBPhotoOtherDetail";

const styleDeletePhotoButton = {
    border:0, 
    borderRadius: "10px",
    backgroundColor: "#00a896",
    color: "white",
    height: "40px",
    width: "100%",
    marginBottom: "15px"
}

class PhotoDetailOther extends Component{
    state = {
        photo: {},    
        allphotos: [],
        otherUserDetail: []

    };

    componentDidMount = () => {

        this.props.getUserDetail(this.props.token);
        let id = this.props.match.params.id;
        const self = this;
        const url = "https://api.temenmakan.online/api/gallery/" + id;
        const auth = "Bearer " + this.props.token;
        axios
          .get(url, { headers: { Authorization: auth } })
          .then(response => {
            self.setState({
              photo: response.data
            });
            // console.log("photos", response.data);
          })
          .catch(err => {
            console.log(err);
          })
      };

    render(){
        const { photo } = this.state;
        const id = this.props.match.params.id;
        if(!id){
            id = this.props.otherUserDetail.id
        }

        if(photo.hasOwnProperty("userID")){
            
            return(
                <div>
                <NavBar/>
                <div className="container-fluid main-content">
                    <div className="row main-content-home">
                    <LSBPhotoOtherDetail userID={ photo.userID }/>
                        <div className="col-md-9 col-xs-8 col-foto-detail">
                            <div className="profile-right" id="foto-background">
                                <div className="row">
                                    <div className="col-md-8" style={{paddingRight:0}}>
                                        <img style={{width:"100%", height:"100%", borderTopLeftRadius:"20px", borderBottomLeftRadius:"20px"}} src={photo.urlGalleryPict} alt="detail photo"/>
                                    </div>
                                    <div className="col-md-4" style={{textAlign:"left"}}>
                                        <div className="row">
                                            <div className="col-md-10" style={{marginTop:"20px"}}>
                                                <p>{photo.caption}</p>
                                            </div>
                                        </div>
                                        <div className="button-detail-photo">
                                            <Link to={"/profileother/gallery/"+photo.userID}><button style={styleDeletePhotoButton}>Back</button></Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
        }
        return ""
    }
}

export default connect("userDetail, token, is_login", actions)(withRouter(PhotoDetailOther));