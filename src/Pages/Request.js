import React, { Component } from "react";
import NavBar from "../Component/navbar";
import LsbwPhoto from "../Component/LSBWPhoto";
import axios from "axios";
import InviteList from "../Component/InviteList";
import { connect } from "unistore/react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Request extends Component {

    state = {
        inviteData: [],
        type: "",
        requestState: "Request",
    }

    componentDidMount = () => {
        //Autorefresh 
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );

        const self = this;
        let token = this.props.token
        //Offer
        axios
            .get("https://api.temenmakan.online/api/user/invite", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ inviteData: response.data });
                self.setState({ type: "request" });
                console.log(response.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }

    tick() {
        this.setState({
            time: new Date().toLocaleString()
        });

        if (this.state.type === "request") {
            this.MyRequest();
        } else {
            this.MyOffer();
        }
    }

    MyRequest = () => {
        const self = this;
        let token = this.props.token
        //Request
        axios
            .get("https://api.temenmakan.online/api/user/invite", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ inviteData: response.data });
                self.setState({ type: "request" });
                console.log(response.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    MyOffer = () => {
        const self = this;
        let token = this.props.token
        //Request
        axios
            .get("https://api.temenmakan.online/api/user/invite/offer", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                self.setState({ inviteData: response.data });
                self.setState({ type: "offer" });
                console.log(response.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    render() {
        let { inviteData } = this.state;
        return (
            <div>
                <NavBar />
                <div class="container-fluid main-content">
                    <div class="row main-content-home">
                        <LsbwPhoto />
                        <div class="col-md-9">
                            <div class="request">
                                {/* <div class="row text-center">
                                    <div class="col-md-6 button-request">
                                        <button type="button" id="pesan-button-myrequest" class="btn btn-info" onClick={this.MyRequest}><strong>My Request</strong></button>
                                    </div>
                                    <div class="col-md-6 button-meetup">
                                        <button type="button" id="pesan-button-myinvitation" class="btn btn-info" onClick={this.MyOffer}><strong>My Invitation</strong></button>
                                    </div>
                                </div> 
                                <div>
                                    {inviteData.map((item, key) => {
                                        if(this.state.type == "offer") {
                                            return <InviteList type={this.state.type} id={item.id} partner_id={item.partner_id} users_id={item.users_id} name={item['users_partner.name']} gender={item['users_partner.gender']}  city={item['users_partner.city.name']} urlPict={item['users_partner.urlPict']} date={item.created_at} status={item.status}/>
                                        }else{
                                            return <InviteList type="request" id={item.id} partner_id={item.partner_id} users_id={item.users_id} name={item['users.name']} gender={item['users.gender']}  city={item['users.city.name']} urlPict={item['users.urlPict']} date={item.created_at} status={item.status} />
                                        }
                                        
                                    })}
                                </div> */}
                                {/* <InviteAction/> */}
                                <div className="row">
                                    <div className="col-md-12">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item text-center">
                                                <a class="nav-link nav-left active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" onClick={this.MyRequest}><i class="fas fa-handshake"></i> MY REQUEST</a>
                                            </li>
                                            <li class="nav-item text-center">
                                                <a class="nav-link nav-right" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" onClick={this.MyOffer}><i class="fas fa-envelope"></i> MY INVITATION</a>
                                            </li>
                                        </ul>
                                    </div>
                                    {/* <div className="col-md-12">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active text-center" id="home">
                                                <h4 className="title-request" style={{paddingTop:'10px'}}>MY REQUEST</h4>
                                            </div>
                                            <div class="tab-pane fade text-center" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                <h4 className="title-request" style={{paddingTop:'10px'}}>MY INVITATION</h4>
                                            </div>
                                        </div>
                                    </div> */}
                                    <div className="col-md-12">
                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                {inviteData.map((item, key) => {
                                                    if (this.state.type === "offer") {
                                                        return <InviteList type={this.state.type} id={item.id} partner_id={item.partner_id} users_id={item.users_id} name={item['users_partner.name']} gender={item['users_partner.gender']} city={item['users_partner.city.name']} urlPict={item['users_partner.urlPict']} date={item.created_at} status={item.status} />
                                                    } else {
                                                        return <InviteList type="request" id={item.id} partner_id={item.partner_id} users_id={item.users_id} name={item['users.name']} gender={item['users.gender']} city={item['users.city.name']} urlPict={item['users.urlPict']} date={item.created_at} status={item.status} />
                                                    }

                                                })}
                                            </div>
                                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                {inviteData.map((item, key) => {
                                                    if (this.state.type === "offer") {
                                                        return <InviteList type={this.state.type} id={item.id} partner_id={item.partner_id} users_id={item.users_id} name={item['users_partner.name']} gender={item['users_partner.gender']} city={item['users_partner.city.name']} urlPict={item['users_partner.urlPict']} date={item.created_at} status={item.status} />
                                                    } else {
                                                        return <InviteList type="request" id={item.id} partner_id={item.partner_id} users_id={item.users_id} name={item['users.name']} gender={item['users.gender']} city={item['users.city.name']} urlPict={item['users.urlPict']} date={item.created_at} status={item.status} />
                                                    }

                                                })}
                                            </div>
                                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </div>
        )
    }
}

export default connect("token")(Request);