import React, { Component } from "react";
import NavBar from "../Component/navbar";
import { withRouter, Link } from "react-router-dom";
import { connect } from "unistore/react";
import { actions } from "../store";
import NavbarProfileOther from "../Component/NavbarProfileOther"
import axios from "axios";
import Truncate from 'react-truncate';
import LSBPhotoOther from "../Component/LSBPhotoOther";

class Post extends Component {
  state = {
    allposts: [],
    urlPict: "",
    posting: "",
    otherUserDetail: []
  };

  componentDidMount = () => {
    this.props.getUserDetail(this.props.token);

    const self = this;
    const url = "https://api.temenmakan.online/api/user/posting/" + this.props.match.params.id;
    const auth = "Bearer " + this.props.token;
    axios
      .get(url, { headers: { Authorization: auth } })
      .then(response => {
        self.setState({
          allposts: response.data
        });
        console.log("posts", response.data);
      })
      .catch(err => {
        console.log(err);
      });

    axios
      .get("https://api.temenmakan.online/api/user/" + this.props.match.params.id, { headers: { "Authorization": auth } })
      .then((response) => {
        self.setState({
          otherUserDetail: response.data
        })
      })
      .catch((err) => {
        console.log(err)
      })
  };

  compareValues = (key, order='asc') => {
    return function(a, b) {
      if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }
  
      const varA = (typeof a[key] === 'string') ?
        a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string') ?
        b[key].toUpperCase() : b[key];
  
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order == 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  render() {
    let id = this.props.match.params.id;
    if (!id) {
      id = this.props.userDetail.id
    }

    const { allposts, otherUserDetail } = this.state;
    return (
      <div>
        <NavBar />
        <div className="container-fluid main-content">
          <div className="row main-content-home">
            <LSBPhotoOther id={id} />
            <div class="col-md-9">
              <div className="profile-right">
                <NavbarProfileOther id={id} />
                <hr />
                <div class="post-box">
                <h5 style={{marginBottom:"40px"}}>Review oleh {otherUserDetail.username}</h5>
                {allposts.sort(this.compareValues('created_at', 'desc')).map((item, key) => {
                    return (
                      <div class="row">
                        <div class="col-md-1 foto-post">
                          <img alt="avatar-user"
                            width="50px"
                            height="50px"
                            src={item['users.urlPict']}
                          />
                        </div>
                        <div class="col-md-11">
                          <div className="row deskripsi-post">
                            <div className="col-md-6">
                              <span>
                                <strong>{item['users.name']}</strong>
                              </span>
                            </div>
                          </div>
                          <div class="post-text short">
                            <Truncate lines={3} 
                            // ellipsis={}
                            >
                              <p>
                                {item.posting}
                              </p>
                            </Truncate>

                          </div>
                          <Link to={"/profileother/post/detail/" + item.id}>
                            <img alt="post-image"
                              class="gambar-post"
                              src={item.urlPict}
                            />
                          </Link>
                        </div>
                      </div>
                    );
                  })}
                  </div>
              </div>
            </div>
                {/* </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  "userDetail, token, is_login",
  actions
)(withRouter(Post));
