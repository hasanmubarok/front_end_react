import React, {Component} from "react";
import NavBar from "../Component/navbar";
import LsbwPhoto from "../Component/LSBWPhoto";
import "../App.css";
import axios from "axios";
import ChatList from "../Component/ChatList";
import {connect} from "unistore/react";
import NotFound from "./NotFound";

class ChatGroup extends Component{

    state = {
        chatData: [],
        message: "",
    }   

    inputChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    componentDidMount = () => {
        const self = this;
        let token = this.props.token
        axios
        .get("https://api.temenmakan.online/api/user/group/chat/"+this.props.match.params.id, {
            headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            self.setState({ chatData: response.data});
            console.log(response.data)
        })
        .catch((err) => {
            console.log(err)
        }) 
    }

    SendChat = () => {        
        const self = this;
        let token = this.props.token
        axios
        .post("https://api.temenmakan.online/api/user/group/chat/"+this.props.match.params.id,{comment: self.state.message},{
            headers: { Authorization: "Bearer " + token }
        })
        .then(function(response){
            // alert("Pesan Terkirim")
        })
        .catch((err) => {
            console.log(err)
        })  
    }

    scrollToBottom() {
        const scrollHeight = this.messageList.scrollHeight;
        const height = this.messageList.clientHeight;
        const maxScrollTop = scrollHeight - height;
        this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }
      
    componentDidUpdate() {
        this.scrollToBottom();
    }

    render(){

        let {chatData} = this.state;
        if (this.props.is_login) {
            return(
                <div>
                    <NavBar/>
                    <div className="container-fluid main-content">
                        <div className="row main-content-home">
                            <LsbwPhoto/>
                            <div className="col-md-9">
                            <div className="group">
                                    <div className="row">
                                        <div className="col-md-12 top-chat ">
                                            <img width="20px" height="20px" src="https://image.flaticon.com/icons/svg/271/271218.svg" />
                                            <h5>Chat</h5>
                                            <div className="avatar-list">
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                                <img alt="user-avatar" src="https://image.flaticon.com/icons/svg/145/145843.svg" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row participant chat-group-participant">
                                        <h4>Group Conversation</h4>
                                        <hr/>
                                        <div className="scrollbar" id="style-2" ref={(div) => {this.messageList = div;}}>
                                            <div className="force-overflow">
                                            {chatData.map((item, key) => {
                                                return <ChatList urlPict={item['users.urlPict']} sender={item['users.name']} message={item.comment} time={item.created_at} id={this.props.match.params.userID} sender_id={item.sender_id}/>
                                            })}
                                            </div>
                                        </div>
                                            <div className="col-md-12 order chat-list">
                                                <div className="conversation-item ">
                                                    <br/>
                                                    <label for="comment">Kirim Pesan:</label><br/>
                                                    <textarea name="message" className="form-control" onChange={this.inputChange} ></textarea><br/>
                                                    <button className="btn btn-info" onClick={this.SendChat}>Kirim</button>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <NotFound />
            )
        }

        
        }
    }
export default connect("is_login, token") (ChatGroup);

