import React from "react";
import { Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";

const Loading = () => <div className="row"><div className="loadings text-center"><img src="https://media.giphy.com/media/lYhl5UtFnPqe4c6To9/giphy.gif"></img></div></div>;

const LandingPage = Loadable({
    loader: () => import(/* webpackChunkName: "landingpage"*/ "../Pages/LandingPage"),
    loading: () => <Loading />
});

const Chat = Loadable({
    loader: () => import(/* webpackChunkName: "chat"*/ "../Pages/Chat"),
    loading: () => <Loading />
});

const Home = Loadable({
    loader: () => import(/* webpackChunkName: "home"*/ "../Pages/Home"),
    loading: () => <Loading />
});

const Profile = Loadable({
    loader: () => import(/* webpackChunkName: "profile"*/ "../Pages/Profile"),
    loading: () => <Loading />
});

const OtherProfile = Loadable({
    loader: () => import(/* webpackChunkName: "otherprofile"*/ "../Pages/OtherProfile"),
    loading: () => <Loading />
});

const Request = Loadable({
    loader: () => import(/* webpackChunkName: "request"*/ "../Pages/Request"),
    loading: () => <Loading />
});

const Referensi = Loadable({
    loader: () => import(/* webpackChunkName: "referensi"*/ "../Pages/Referensi"),
    loading: () => <Loading />
})

const Post = Loadable({
    loader: () => import(/* webpackChunkName: "referensi"*/ "../Pages/Post"),
    loading: () => <Loading />
})

const Gallery = Loadable({
    loader: () => import(/* webpackChunkName: "gallery"*/ "../Pages/Gallery"),
    loading: () => <Loading />
})

const GroupCreate = Loadable({
    loader: () => import(/* webpackChunkName: "group"*/ "../Pages/CreateGroup"),
    loading: () => <Loading />
})

const ChatGroup = Loadable({
    loader: () => import(/* webpackChunkName: "chatgroup"*/ "../Pages/ChatGroup"),
    loading: () => <Loading />
})

const Load = Loadable({
    loader: () => import(/* webpackChunkName: "loading"*/ "../Component/Loading"),
    loading: () => <Loading />
})

const NotFound = Loadable({
    loader: () => import(/* webpackChunkName: "notfound"*/ "../Pages/NotFound.js"),
    loading: () => <Loading />
})

const PostDetail = Loadable({
    loader: () => import(/* webpackChunkName: "postdetail"*/ "../Pages/PostDetail"),
    loading: () => <Loading />
})

const PostAllDetail = Loadable({
    loader: () => import(/* webpackChunkName: "postalldetail"*/ "../Pages/PostAllDetail"),
    loading: () => <Loading />
})

const Register = Loadable({
    loader: () => import(/* webpackChunkName: "register"*/ "../Pages/Register"),
    loading: () => <Loading />
})

const PetunjukKeamanan = Loadable({
    loader: () => import(/* webpackChunkName: "petunjukkeamanan*/ "../Pages/PetunjukKeamanan"),
    loading: () => <Loading />
})

const PostProfile = Loadable({
    loader: () => import(/* webpackChunkName: "postsendiri*/ "../Pages/PostProfile"),
    loading: () => <Loading />
})


const PostOtherProfile = Loadable({
    loader: () => import(/* webpackChunkName: "postother*/ "../Pages/PostOtherProfile"),
    loading: () => <Loading />
})

const UpdateProfile = Loadable({
    loader: () => import(/* webpackChunkName: "updateprofile*/ "../Pages/UpdateProfile"),
    loading: () => <Loading />
})

const PostOtherProfileDetail = Loadable({
    loader: () => import(/* webpackChunkName: "postotherprofiledetail*/ "../Pages/PostOtherProfileDetail"),
    loading: () => <Loading />
})

const GalleryOtherProfile = Loadable({
    loader: () => import(/* webpackChunkName: "updateprofile*/ "../Pages/GalleryOther"),
    loading: () => <Loading />
})

const AboutUs = Loadable({
    loader: () => import(/* webpackChunkName: "aboutus*/ "../Pages/AboutUs"),
    loading: () => <Loading />
})

const ReferensiOther = Loadable({
    loader: () => import(/* webpackChunkName: "referensiother*/ "../Pages/ReferensiOther"),
    loading: () => <Loading />

})

const PhotoDetail = Loadable({
    loader: () => import(/* webpackChunkName: "photodetail*/ "../Pages/PhotoDetail"),
    loading: () => <Loading />
})

const PhotoDetailOther = Loadable({
    loader: () => import(/* webpackChunkName: "photodetailother*/ "../Pages/PhotoDetailOther"),
    loading: () => <Loading />
})

const Following = Loadable({
    loader: () => import(/* webpackChunkName: "following*/ "../Pages/Following"),
    loading: () => <Loading />
})

const Follower = Loadable({
    loader: () => import(/* webpackChunkName: "follower*/ "../Pages/Follower"),
    loading: () => <Loading />
})

const OtherFollowing = Loadable({
    loader: () => import(/* webpackChunkName: "otherfollowing*/ "../Pages/OtherFollowing"),
    loading: () => <Loading />
})

const OtherFollower = Loadable({
    loader: () => import(/* webpackChunkName: "otherfollower*/ "../Pages/OtherFollower"),
    loading: () => <Loading />
})

const Group = Loadable({
    loader: () => import(/* webpackChunkName: "otherfollower*/ "../Pages/Group"),
    loading: () => <Loading />
})

const Search = Loadable({
    loader: () => import(/* webpackChunkName: "search*/ "../Pages/Search"),
    loading: () => <Loading />
})

const TrialMap = Loadable({
    loader: () => import(/* webpackChunkName: "trialmap*/ "../Pages/TrialMap"),
    loading: () => <Loading />
})

const MainRoute = () => {
    return(
        <Switch>
            <Route exact path="/" component={LandingPage}/>
            <Route exact path="/home" component={Home}/>
            <Route exact path="/chat/:userID" component={Chat}/>
            <Route exact path="/profileother/referensi/:id" component={ReferensiOther}/>
            <Route exact path="/profile/tentang" component={Profile}/>
            <Route exact path="/post" component={Post}/>
            <Route exact path="/request" component={Request}/>
            <Route exact path="/profile/referensi/:id" component={Referensi}/>
            <Route exact path="/profile/referensi" component={Referensi}/>
            <Route exact path="/profileother/tentang/:id" component={OtherProfile}/>
            <Route exact path="/profileother/post/:id" component={PostOtherProfile}/>
            <Route exact path="/profileother/post/detail/:id" component={PostOtherProfileDetail}/>
            <Route exact path="/profileother/gallery/detail/:id" component={PhotoDetailOther}/>
            <Route exact path="/profileother/gallery/:id" component={GalleryOtherProfile}/>
            <Route exact path="/profile/referensi" component={Referensi}/>
            <Route exact path="/profile/post/:id" component={PostProfile}/>
            <Route exact path="/profile/post" component={PostProfile}/>
            <Route exact path="/profile/gallery" component={Gallery}/>
            <Route exact path="/profile/gallery/:id" component={PhotoDetail}/>
            <Route exact path="/post/:id" component={PostDetail}/>
            <Route exact path="/post/all/:id" component={PostAllDetail}/>
            <Route exact path="/request" component={Request}/>
            <Route exact path="/group/:id" component={Group}/>
            <Route exact path="/group" component={GroupCreate}/>
            <Route exact path="/group/chat/:id" component={ChatGroup}/>
            <Route exact path="/loading" component={Load}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/petunjuk" component={PetunjukKeamanan}/>
            <Route exact path="/updateprofile" component={UpdateProfile}/>
            <Route exact path="/aboutus" component={AboutUs}/>
            <Route exact path="/profile/following" component={Following}/>
            <Route exact path="/profile/follower" component={Follower}/>
            <Route exact path="/profileother/following/:id" component={OtherFollowing}/>
            <Route exact path="/profileother/follower/:id" component={OtherFollower}/>
            <Route exact path="/search/:query" component={Search}/>
            <Route exact path="/search" component={Search}/>
            <Route exact path="/profileotherpostdetail" component={PostOtherProfileDetail}/>
            <Route exact path="/trialmap" component={TrialMap}/>
            <Route component={NotFound}/>
        </Switch>
    )
}

export default MainRoute;