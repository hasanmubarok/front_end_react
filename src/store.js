import createStore from "unistore";
import devtools from "unistore/devtools";
import axios from "axios";
import { withRouter } from 'react-router-dom';
import persistStore from "unissist";
import localStorageAdapter from 'unissist/integrations/localStorageAdapter';
import swal from 'sweetalert2';


const initialState = {
    userList: [],
    userDetail: [],
    token: '',
    is_login: false,
    userAvailable:[],
    longitude : "",
    latitude : "",
}

const store =
process.env.NODE_ENV === "production"? createStore(initialState) : devtools(createStore(initialState));

const adapter = localStorageAdapter();
persistStore(store, adapter);

const actions = store => ({
    // registerHandle: async(state, username, email, password, name, gender, dateofBirth)

    getLocation : async (state, longitude, latitude)=> {
        // alert("ui")
        store.setState({
            longitude: longitude,
            latitude: latitude,
        })
    },

    getUserAvailable : async (state)=> {
        const self = this;
        let token = this.props.token
        axios
            .get("https://api.temenmakan.online/api/users", {
                headers: { Authorization: "Bearer " + token }
            })
            .then(function (response) {
                store.setState({
                    userAvailable: response.data.users,
                })
                console.log(response.data)
            })
    },

    signInHandle: async (state, username, password, latitude, longitude) => {
        
        const url = "https://api.temenmakan.online/api/user/login"

        const body = {
            username: username,
            password: password,
            latitude: latitude,
            longitude: longitude
        }
        await axios
        .post(url, body)
        .then((response) => {
            // alert("Selamat datang " + username + "!")
            swal(
                'Selamat datang '+ username + '!'
              )
            store.setState({
                token: response.data.token,
                is_login: true
            })
            console.log("Response: ", response)
            // this.props.history.push('/home');
        })
        .catch((err) => {
            console.log(err)
            // alert("Username/password salah!")
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'User atau password salah'
              })
        })  
    },

    getUserDetail: async (state, token) => {
        const url = "https://api.temenmakan.online/api/user/me"
        const auth = "Bearer " + token
        await axios
        .get(url, { headers: { "Authorization": auth } })
        .then((response) => {
            store.setState({
                userDetail: response.data
            })
        })
        .catch((err) => {
            console.log(err)
        })
    },

    handleLogout: (state) => {
        store.setState({
            token: '',
            is_login: false,
            userDetail: ''
        })
    }
})

export { store, withRouter,actions }